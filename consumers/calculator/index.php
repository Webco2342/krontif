<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("расчет тепловой мощности котла");
?>
<p>
Мощность котла является одной из важнейших характеристик отопительного оборудования. Избыток мощности обернется переплатой за котел, недостаток – невозможностью котла отопить необходимую площадь помещения.</p>

Точный расчет мощности необходим для:
<ul class="krontif_ul">
       <li> Оптимизации расходов топлива;</li>

       <li> Минимизации затрат на топливо;</li>

        <li>Поддержания комфортной температуры в отапливаемом помещении.</li>
</lu>
<p>
Как правило в паспортах на отопительные котлы указывают мощность и обогреваемую площадь исходя из среднестатистических стандартов расчета: 1кВт на 10м2 (при высоте потолка до 2,7 м). Но при данном подходе не учитываются возможные теплопотери, которые влияют на конечную мощность котла, и не учитываются особенности климата того или иного региона.
</p>
</div>
</div>
</div>
</div>
<div class="red_title_bg">
<div class="container">
<div class="row red_contet">
<img src="inter.png"> ЭТО ИНТЕРЕСНО! 
</div>
</div>
</div>
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			
<p><strong>Теплопотери отапливаемой площади зависят от: </strong></p>
<ul class="krontif_ul">
       <li>Материала и толщины наружных стен </li>

       <li>Типа и количества окон </li>

       <li>От типа чердачного помещения </li>

       <li>От количества этажей в помещении, и прочее.</li>
</ul>


<center><div class="h3">Простейшая формула расчета мощности котла выглядит так:</div>

<div class="h2">Wкт = Sпм х Wуд/10, где</div></center>

<p><b>Wкт</b> – расчетная мощность котла, кВт;</p>

<p><b>Sпм</b> — площадь дома, м²;</p>

<p><b>Wуд</b> – поправочный коэффициент, кВт/ м². (поправка на климатические условия отдельного региона).</p>

	   <div style="padding:15px"><center>Значения поправочных коэффициентов <b>(Wуд)</b>:</center></div>
	   
<p>- для Центральной европейской части страны – от 1,2 до 1,5</p>

<p>- для Южных регионов России – от 0,7 до 0,9</p>

<p>- для Северных регионов России – от 1,5 до 2	</p>   


<div style="color:#747474;margin:15px;">

<center>
Для того, чтобы сделать предварительный выбор котла и заранее ознакомиться с ценами перед покупкой, Вы можете рассчитать ориентировочную мощность котла, воспользовавшись нашим 
<p style="margin-bottom:20px; color:#000;"><b>
Онлайн-калькулятором:
</b></p>
</div>
</center>

</p>
</div>
</div>
</div>
</div>
<script language="JavaScript" type="text/JavaScript">
 <!--
 function calcTeplopoter()
 { 
 var itogValue = 0.1 * fm_clc.s1.value * fm_clc.s2.value * fm_clc.s3.value * fm_clc.s4.value * fm_clc.s5.value * fm_clc.s6.value * fm_clc.s7.value * 1.0 * fm_clc.s9.value ;
 fm_clc.teplopoter.value = itogValue.toFixed(2);
 var itogPoter = (itogValue * 1.2) -  fm_clc.teplopoter.value;
 fm_clc.poter.value = itogPoter.toFixed(2);

 fm_clc.itog_moshn.value = (itogValue * 1.2).toFixed(2);
 }
 //-->

 </script>
 
<div class="bg_catalog_index_hit_clic calcul">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form class="form-horizontal" role="form" method="POST" name="fm_clc" action="" id="calcform" >
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Площадь помещения:</div>
						<input type="text" name="s9" id="s9" class="col-xs-1" value="100" required=""> <div class="col-xs-1"> м<sup>2</sup></div>		
						<div class="col-xs-1"></div><div class="col-xs-3 form_title">Мощность котла без учета теплопотерь:</div>
					</div>
					
					 
					
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Высота помещения:</div>
						<select class="col-xs-2" name="s7" id="s7">
						  <option value="1.00">2,5</option>
						  <option value="1.05" selected="selected">3,0</option>
						  <option value="1.10">3,5</option>
						  <option value="1.15">4,0</option>
						  <option value="1.20">4,5</option>
						</select>
						<div class="col-xs-1"></div><input id="teplopoter" size="15" value="0" class="col-xs-2 form_result" name="teplopoter" /> <div class="col-xs-1">кВт.</div>
						
					</div>
					
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Стены:</div>
					
						<select class="col-xs-2" name="s2" id="s2">
						  <option value="0.85" selected="selected">Хорошая изоляция</option>
						  <option value="1.0">Ж/бетон, кирпич (2), утеплитель (150 мм)</option>
						  <option value="1.27">Плохая изоляция</option>
						</select>
						<div class="col-xs-1"></div><div class="col-xs-3 form_title">Теплопотери: </div>
					</div>
					<div class="form-group input">	<div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Соотношение площадей окон и пола:</div>
					
						<select class="col-xs-2" name="s3" id="s3">
						  <option value="0.80" selected="selected">10%</option>
						  <option value="0.90">11%-19%</option>
						  <option value="1.00">20%</option>
						  <option value="1.10">21%-29%</option>
						  <option value="1.20">30%</option>
						  <option value="1.30">31%-39</option>
						  <option value="1.40">40%</option>
						  <option value="1,50">50%</option>
						</select>						
						<div class="col-xs-1"></div><input id="poter" name="poter" value="0" class="col-xs-2 form_result" size="15" /> <div class="col-xs-1">кВт.</div>
					</div>
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Тип окон:</div>
					
						<select id="s1" class="col-xs-2" name="s1">
						  <option value="0.85">Тройной стеклопакет</option>
						  <option value="1.0">Двойной стеклопакет</option>
						  <option value="1.27" selected="selected">Обычное (двойное) остекление</option>
						</select>
						<div class="col-xs-1"></div><div class="col-xs-3 form_title">Необходимая мощность котла с учетом теплопотерь: </div>
					</div>
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Число стен, выходящих наружу:</div>
					
						<select id="s5" class="col-xs-2" name="s5">
						  <option value="1.00">Одна</option>
						  <option value="1.11">Две</option>
						  <option value="1.22">Три</option>
						  <option value="1.33" selected="selected">Четыре</option>
						</select>
						<div class="col-xs-1"></div><input id="itog_moshn" name="itog_moshn" value="0" class="col-xs-2 form_result" size="15" /> <div class="col-xs-1">кВт.</div>
					</div>
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Температура снаружи помещения:</div>
					
						<select class="col-xs-2" name="s4" id="s4">
						  <option value="0.70">до -10</option>
						  <option value="0.80" selected="selected">-10</option>
						  <option value="0.90">-15</option>
						  <option value="1.00">-20</option>
						  <option value="1.10">-25</option>
						  <option value="1.20">-30</option>
						  <option value="1.30">-35</option>
						</select>
					</div>	
					
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Тип помещения над расчитываемым:</div>
					
						<select class="col-xs-2" name="s6" id="s6">
						  <option value="0.82">Обогреваемое помещение</option>
						  <option value="0.91" selected="selected">Теплый чердак</option>
						  <option value="1.00">Холодный чердак</option>
						</select>
					</div>
					
					<div class="right">
						<div id="btncalc" onclick="return calcTeplopoter()" >
						<svg class="pie_form" version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">		
						<defs>
							<linearGradient id="gradient">
								<stop offset="0%" style="stop-color: #ed1c24"></stop>
								<stop offset="100%" style="stop-color: #97080e"></stop>
							</linearGradient>
						</defs>
					<path class="tr_el_1" d="M 0 45 L 335 0 L 100 155  z"> </path>
					<path class="tr_el_2" fill="url(#gradient)" d="M 60 25 L 250 25 L 250 107 L 32 107  z"> </path>
					<text x="71" y="75" fill="#fff" font-size="30">РАССЧИТАТЬ </text>
					</svg>	
						
						</div>
					</div>
					

				</form>	
				</div>
			</div>
		</div>
	</div>	
</div>				
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			
			      <div style="color:#ed2e3e; font-size:20px;">     <img src="vn.png">     ВНИМАНИЕ! </div>
<p style="padding:10px 0">
Расчет оптимальной мощности с учетом всех параметров, влияющих на конечное значение мощности, требует специализированных знаний. <br>
<b>Доверяйте расчет мощности вашего будущего котла квалифицированным профессионалам.</b>
</p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>