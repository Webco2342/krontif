<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация котла");
if(CModule::IncludeModule("iblock"))
?>



<center><h3>Регистрация продукции «KRONTIF» - это легкий и быстрый доступ<br> 
к целому ряду возможностей:</h3></center>


			</div>
		</div>
	</div>
</div>

<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			
<div class="cal_bloc">
<center><div><img src="cal_ico_1.png"></div>	Зарегистрировав котел в течение 1 месяца с момента покупки, Вы получаете <br>
<b>1 ГОД ДОПОЛНИТЕЛЬНОЙ ГАРАНТИИ*</b></center>
</div>

<div class="col-md-12 cal_bloc">
<div class="col-md-1">
<img src="cal_ico_2.png">
</div>
<div class="col-md-5">
Горячая линия поддержки <br>
<span>8 800 550 28 20</span>
</div>	

<div class="col-md-1">
<img src="cal_ico_3.png">
</div>
<div class="col-md-5">
Последние новости <b>АО «КРОНТИФ-ЦЕНТР»</b> <br>
о новых акциях и продуктах
</div>	
</div>


<div class="col-md-12 cal_bloc">
<div class="col-md-1">
<img src="cal_ico_4.png">
</div>
<div class="col-md-5">
Членство в сообществе <b>«KRONTIF»</b>:<br>  участие в опросах клиентов о качестве<br> продукции

</div>	

<div class="col-md-1">
<img src="cal_ico_5.png">
</div>
<div class="col-md-5">
Возможность участия в лотереях и<br> конкурсах, проводимых <br>
<b>АО «Кронтиф-центр»</b>, и возможность<br> выигрывать достойные подарки
</div>	
</div>

<div class="cal_bloc">
<p><b>ВНИМАНИЕ!</b> Право на дополнительную гарантию НЕ предоставляется в случае, если котел не был зарегистрирован в течение 1 месяца с момента Покупки;</p>

<p>* После успешной регистрации Вам на почту будет отправлен Купон, дающий право на дополнительную гарантию. Предъявляйте его в наших сервисных центрах/или в точке продажи/или напрямую на завод. Условия вступления в силу Гарантийных обязательств читайте <a href="/consumers/service/guarantee/">здесь</a>.</p>
</div>
			</div>
		</div>
	</div>
</div>

<div class="bg_catalog_index_hit_clic">	
	<div class="container">
		<div class="row">
			<div class="col-md-12 cal_bloc">
	<div class="col-md-2"></div>
			<div class="col-md-8">
				<form class="form-horizontal" role="form"  method="POST" id="registrationkrontif" action="javascript:void(null);" onsubmit="call()">
				<div class="form-group">
					<p>Модель котла: *</p>
				<select class="col-xs-12" name="s7" id="s7">						  
						  <option value="1.05" selected="selected">Модель котла</option>
				<?

$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>7, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID"=>Array(2155, 2154));
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
?>		  
						  <option value="<?=$arFields["NAME"]?>"><?=$arFields["NAME"]?></option>
 <?

}
?>
						</select>
				</div>
					<div class="form-group">
					<p>Серийный номер: *</p>
						<input type="text" name="number" class="col-xs-12" placeholder="Серийный номер" required>
					</div>
					<div class="form-group">
					<p>Дата выпуска котла: *</p>
						<input type="date" name="date" class="col-xs-12" placeholder="00.00.0000" required>
					</div>
					<div class="form-group">
					<p>Ваше имя: *</p>
						<input type="text" name="name" class="col-xs-12" placeholder="Ваше имя" required>
					</div>	
					<div class="form-group">
					<p>Ваше фамилия: *</p>
						<input type="text" name="female" class="col-xs-12" placeholder="Ваше фамилия" required>
					</div>	
					<div class="form-group">
					<p>Регион: *</p>
						<input type="text" name="reg" id="region" autocomplete="off" class="col-xs-12" placeholder="Регион" required role="textbox" aria-autocomplete="list" aria-haspopup="true">
					</div>
					<div class="form-group">
					<p>Город: *</p>
						<input type="text" name="cit" id="cit" autocomplete="off" class="col-xs-12" placeholder="Город" required role="textbox" aria-autocomplete="list" aria-haspopup="true">
					</div>
					<div class="form-group">
					<p>Телефон: *</p>
						<input type="text" name="phone" id="mtel" class="col-xs-12" placeholder="Телефон" required>
					</div>
					<div class="form-group">
					<p>Email: *</p>
						<input type="email" id="mail" name="email" class="col-xs-12" placeholder="Email" required>
					</div>					
					<div class="form-group">
					<p>
					Дополнительная информация:
					</p>
						<textarea class="col-xs-12" name="text" rows="3"></textarea>
					</div>
					<div class="form-group">
					 * Поля обязательные для заполнения
					 </div>
					<div class="form-group">
						<button type="submit" class="btn btn-danger">Отправить</button>
					</div>
<div id="results">
</div>
				</form>		


				<script type="text/javascript" language="javascript">
					function call() {
					  var msg   = $('#registrationkrontif').serialize();
						$.ajax({
						  type: 'POST',
						  url: 'reg-ajax.php',
						  data: msg,
						  success: function(data) {
							$('#results').html(data);
						  },
						  error:  function(xhr, str){
						alert('Возникла ошибка: ' + xhr.responseCode);
						  }
						});
						}
					
				</script>
				<script>
							window.onload = function() {
										var availableReg = [

									<? 

									 $SectList = CIBlockSection::GetList($arSort, array("IBLOCK_CODE"=>'CITY', "SECTION_ID"=>"2075", "ACTIVE"=>"Y") ,false, array("ID","IBLOCK_ID","NAME"));
											while ($SectListGet = $SectList->GetNext())
											{
												echo "'".$SectListGet['NAME']."', ";
											}   

									?>
									];
									jQuery( "#region" ).autocomplete({source: availableReg});

										var availableCity = [

									<?
									$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
									$arFilter = Array("IBLOCK_CODE"=>'CITY', "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
									$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5000), $arSelect);
									while($ob = $res->GetNextElement())
									{
									 $arFields = $ob->GetFields();
									 echo "'".$arFields['NAME']."', ";
									}
									?> 
										];
									jQuery( "#cit" ).autocomplete({source: availableCity});
								}				
			</script>
			

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>