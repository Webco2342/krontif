<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("добавить точку продаж");
if(CModule::IncludeModule("iblock"))
?>

			<div class="col-md-12 cal_bloc">
	<div class="col-md-2"></div>
			<div class="col-md-8">
				<form class="form-horizontal" role="form" enctype="multipart/form-data"  method="POST" id="registrationkrontif" action="partner-reg-ajax.php" onsubmit="call()">
					<div class="form-group">
					<p>Юридическое лицо: *</p>
						<input type="text" name="company" id="company" class="col-xs-12" placeholder="ООО КОТЕЛ" >
					</div>
					<div class="form-group">
					<p>Регион: *</p>
						<input type="text" name="reg" id="region" autocomplete="off" class="col-xs-12" placeholder="Регион"  role="textbox" aria-autocomplete="list" aria-haspopup="true">
					</div>
					<div class="form-group">
					<p>Город: *</p>
						<input type="text" name="cit" id="cit" autocomplete="off" class="col-xs-12" placeholder="Город"  role="textbox" aria-autocomplete="list" aria-haspopup="true">
					</div>	
					<div class="form-group">					
					<p>Название магазина: *</p>
						<input type="text" name="name-magazine" class="col-xs-12" placeholder="Магазин" >
					</div>
											
					<div class="form-group">					
					<p>Тип дилера:</p>	
						<div class="col-xs-12 form-group-st">
						<label> 
							<input type="checkbox" class="checkbox" name="type" value="Территориальный дистрибьютор"> 
							<span class="checkbox-custom"></span>
							<span class="label">Территориальный дистрибьютор</span> 
						</label>
						</div>
						<div class="col-xs-12 form-group-st">
						<label>
							<input type="checkbox" class="checkbox" name="type1" value="Оптовые продажи"> 
							<span class="checkbox-custom"></span>
							<span class="label">Оптовые продажи</span> 
						</label>
						</div>
						<div class="col-xs-12 form-group-st">
						<label>
							<input type="checkbox" class="checkbox" name="type2" value="Розничная сеть"> 
							<span class="checkbox-custom"></span>
							<span class="label">Розничная сеть</span> 
						</label>
						</div>
						<div class="col-xs-12 form-group-st">
						<label>
							<input type="checkbox" class="checkbox" name="type3" value="Розничный магазин"> 
							<span class="checkbox-custom"></span>
							<span class="label">Розничный магазин</span> 
						</label>
						</div>
						<div class="col-xs-12 form-group-st">
						<label>
							<input type="checkbox" class="checkbox" name="type4" value="Интернет-магазин"> 
							<span class="checkbox-custom"></span>
							<span class="label">Интернет-магазин</span> 
						</label>
						</div>
					</div>	
					<div class="form-group">					
					<p>Адрес: *</p>
						<div class="col-xs-1">Улица</div> <input type="text" name="adres" class="col-xs-4" id="adres_ul" placeholder="ул.Ленина" > 
						<div class="col-xs-1">Дом</div> <input type="text" id="adres_dom" name="adresdom" class="col-xs-2" placeholder="1" >
					</div>					
					<div class="form-group">
					<div id="searmap" class="btn btn-danger">найти на карте</div>
					</div>
					<div id="results-maps">
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">
    </script>


 <script type="text/javascript">
        ymaps.ready(init);
      
        function init(){ 
            var myMap = new ymaps.Map("map", {
                center: [55.734037, 37.720076],
                zoom: 12
            }); 
            
            
            
            myMap.geoObjects.add(myPlacemark);
        }
    </script>
	
	 <div id="map" style="width: 300px; height: 300px"></div>
					</div>				

					<div class="form-group">					
					<p>Телефоны: *</p>					
						<div class="col-xs-12 form-group-st"><input type="text" name="phone0" id="mtel0" class="col-xs-12 mtel" placeholder="Телефон" required></div>
						<div class="col-xs-12 form-group-st"><input type="text" name="phone1" id="mtel1" class="col-xs-12 mtel" placeholder="Телефон" ></div>
						<div class="col-xs-12 form-group-st"><input type="text" name="phone2" id="mtel2" class="col-xs-12 mtel" placeholder="Телефон" ></div>
						<div class="col-xs-12 form-group-st"><input type="text" name="phone3" id="mtel3" class="col-xs-12 mtel" placeholder="Телефон" ></div>
						<div class="col-xs-12 form-group-st"><input type="text" name="phone4" id="mtel4" class="col-xs-12 mtel" placeholder="Телефон" ></div>
					</div>
					<div class="form-group">					
					<p>Режим работы: *</p>
						<div class="col-xs-12 form-group-st"><input type="text"  name="rad-magazine-1-1" class="col-xs-2" placeholder="пн-пт" >
						<div class="col-xs-1"></div>
						<input type="text" name="rad-magazine-1-2" class="col-xs-4" placeholder="09:00-18:00" > </div>
						
						<div class="col-xs-12 form-group-st"><input type="text"  name="rad-magazine-2-1" class="col-xs-2" placeholder="сб" >
						<div class="col-xs-1"></div>
						<input type="text" name="rad-magazine-2-2" class="col-xs-4" placeholder="10:00-17:00" > </div>
						
						<div class="col-xs-12 form-group-st"><input type="text"  name="rad-magazine-3-1" class="col-xs-2" placeholder="вс" >
						<div class="col-xs-1"></div>
						<input type="text" name="rad-magazine-3-2" class="col-xs-4" placeholder="выходной" > </div>
					</div>	
					<div class="form-group">					
					<p>Сайт:</p>					
						<div class="col-xs-12 form-group-st"><input type="text" name="site" id="site" class="col-xs-12" placeholder="www.site.ru" ></div>

					</div>
					<div class="form-group">
					<div class="col-xs-8">Фото фасада</div><input type="file" id="file" class="col-xs-4 file_upload" name="image1" multiple>	
					</div>		
					<div class="form-group">
					<div class="col-xs-8">Фото торгового зала с продукцией Кронтиф</div><input type="file" id="file2" class="col-xs-4 file_upload" name="image2" multiple>	
					</div>	
					<div class="col-xs-12 form-group-st">
					<label>
							<input type="checkbox" class="checkbox" name="type" value="Интернет-магазин" required> 
							<span class="checkbox-custom"></span>
							<span class="label">Я ознакомлен с пользовательским соглашением * </span> 
					</label>
					</div>					
					<div class="form-group">
						<button type="submit" name="2" class="btn btn-danger">Отправить</button>
					</div>
					<div class="col-xs-12 form-group">
					 * Поля обязательные для заполнения
					 </div>
					 <div class="col-xs-12 form-group">
					 Отправляя форму, вы соглашаетесь на обработку персональных данных
					 </div>
<div id="results">
</div>

				</form>		
<script type="text/javascript">
jQuery('#searmap').click(function(){
var ul;
var dom;
var region;
var cit;
var url;
var company;
region = jQuery('#region').val();
cit = ', город ' + jQuery('#cit').val();
ul = ', улица ' + jQuery('#adres_ul').val();
dom = ', дом ' + jQuery('#adres_dom').val();



$.get(
  "/partners/partner-maps-ajax.php",
  {
    region: region,
    cit: cit,
	ul: ul,
	dom: dom,
	company: company
  },
  onAjaxSuccess
);
 
function onAjaxSuccess(data)
{  

$('#results-maps').html(data);
						  
}
						
});

</script>

				<script type="text/javascript" language="javascript">
					
$(function(){
  $('#registrationkrontif').on('submit', function(e){
    e.preventDefault();
    var $that = $(this),
    formData = new FormData($that.get(0));				  
						$.ajax({						  
						  url: $that.attr('action'),
						  type: $that.attr('method'),
						  contentType: false, 
						  processData: false, 
						  data: formData,
						      
						  success: function(data) {
							$('#results').html(data);
						  },
						 
						});
						});
						});
					
				</script>
				<script>
							window.onload = function() {
										var availableReg = [

									<? 

									 $SectList = CIBlockSection::GetList($arSort, array("IBLOCK_CODE"=>'CITY', "SECTION_ID"=>"2075", "ACTIVE"=>"Y") ,false, array("ID","IBLOCK_ID","NAME"));
											while ($SectListGet = $SectList->GetNext())
											{
												echo "'".$SectListGet['NAME']."', ";
											}   

									?>
									];
									jQuery( "#region" ).autocomplete({source: availableReg});

										var availableCity = [

									<?
									$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
									$arFilter = Array("IBLOCK_CODE"=>'CITY', "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
									$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5000), $arSelect);
									while($ob = $res->GetNextElement())
									{
									 $arFields = $ob->GetFields();
									 echo "'".$arFields['NAME']."', ";
									}
									?> 
										];
									jQuery( "#cit" ).autocomplete({source: availableCity});
								}	

								
			</script>				
</div></div></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>