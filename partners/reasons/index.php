<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("7 причин сотрудничать с АО \"КРОНТИФ-ЦЕНТР\"");
?>

			</div>
		</div>
	</div>
</div>
<div class="seven_pchin">
	<div class="container">
			<div class="seven_content_top">
<svg class="seven_svg_left" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="0,0 100,150 0,300"/>
</svg>
<center><p style="margin:20px 0; font-size: 20px;">АО «Кронтиф-Центр» («Сукремльский чугунолитейный завод») одно из ведущих отечественных предприятий по производству чугунного литья, сотрудничество с которым уже выбрали</p>

<p style="font-size: 30px;"><b>1197</b> КЛИЕНТОВ</p>
<div style="font-size: 30px;margin:20px 0">
<p style="color:#e42426;">Присоединяйтесь! </p>
<p>Давайте строить взаимовыгодные отношения, </p>
<p>вместе зарабатывать и радовать потребителей отличным продуктом!</p>
</div>
</center>

<svg class="seven_svg_right" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="100,0 0,150 100,300"/>
</svg>

			</div>
	</div>
</div>

<div class="bg_content">
	<div class="container">
		<div class="row">

<div class="col-md-4 seven_list">
<div class="seven_list_title">1</div>
<h3>МЫ -<br> КРУПНОЕ ПРОИЗВОДСТВЕННОЕ ПРЕДПРИЯТИЕ.</h3>
В России всего два производителя чугунных твердотопливных котлов. Мы – одни из них. Наше предприятие показывает устойчивый рост, мощный производственный потенциал и амбициозные планы развития. 
<strong>С НАМИ СТАБИЛЬНО!</strong>
</div>

<div class="col-md-4 seven_list">
<div class="seven_list_title">2</div>
<h3>МЫ –<br> КЛИЕНТООРИЕНТИРОВАННЫЕ.</h3>
Мы ценим своих Партнеров и каждому Клиенту предоставляем персонального менеджера, который быстро и эффективно решит любую проблему. 
<strong>С НАМИ ТЕПЛО!</strong>
</div>

<div class="col-md-4 seven_list">
<div class="seven_list_title">3</div>
<h3>МЫ –<br> ВЫСОКОТЕХНОЛОГИЧНЫЕ.</h3>
Наше современное европейское оборудование позволяет выпускать технологически сложные изделия, обеспечивая при этом минимальные сроки производства и максимальные объемы поставок. 
<strong>С НАМИ НАДЕЖНО!</strong>
</div>

<div class="col-md-4 seven_list">
<div class="seven_list_title">4</div>
<h3>МЫ ИМЕЕМ СОБСТВЕННУЮ 
СЫРЬЕВУЮ БАЗУ.</h3>
Наш завод входит в состав холдинга ПМХ – мирового лидера по производству высококачественного товарного чугуна, из которого мы производим нашу продукцию. Поэтому мы смело можем гарантировать качество каждого котла KRONTIF.  
<strong>С НАМИ ПРЕСТИЖНО!</strong>
</div>

<div class="col-md-4 seven_list">
<div class="seven_list_title">5</div>
<h3>МЫ ПРОИЗВОДИМ АКТУАЛЬНУЮ ПРОДУКЦИЮ. </h3>
Низкий уровень газификации страны и дорогая электроэнергия вынуждают потребителей искать альтернативные способы отопления такие, как твердотопливные котлы. «Чугунная классика» стала доступнее по цене для населения, не потеряв в качестве. 
<strong>С НАМИ НА ВОЛНЕ!</strong>
</div>

<div class="col-md-4 seven_list">
<div class="seven_list_title">6</div>
<h3>МЫ ОТКРЫТЫ К НОВЫМ ИДЕЯМ. </h3>
Если у Вас есть интересный проект, в котором мы могли бы поучаствовать, мы с удовольствием его рассмотрим. 
<strong>С НАМИ ЛЕГКО!</strong>
</div>

<div class="col-md-4 seven_list">

</div>

<div class="col-md-4 seven_list">
<div class="seven_list_title">7</div>
<h3>МЫ – ВЫГОДНЫЕ. </h3>
Благодаря грамотной ценовой политике мы даем Вам возможность хорошо зарабатывать. 
<strong>С НАМИ ПРИБЫЛЬНО!</strong>
</div>

		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>