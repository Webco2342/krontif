<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия сотрудничества");
?>
<center>
<ul class="partner-menu nav navbar-nav">

<li class="active">
<a href="index.php">
<div>Что получают 
наши Партнеры</div>
</a>
</li>

<li>
<a href="priznaki.php">
<div>7 признаков
идеального партнера</div>
</a>
</li>

<li>
<a href="stat-partnerom.php">
<div>стать  партнером</div>
</a>
</li>

		
</ul>
</center>
			</div>
		</div>
	</div>
</div>



<div class="seven_pchin">
	<div class="container">
			<div class="seven_content_top">
<svg class="seven_svg_left" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="0,0 100,150 0,300"/>
</svg>
<center>
<div style="font-size: 30px">

<p>Став Партнером АО «Кронтиф-центр»,  </p>
<p>вы получаете следующие привилегии: </p>

</div>
</center>

<svg class="seven_svg_right" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="100,0 0,150 100,300"/>
</svg>

			</div>
	</div>
</div>

<div class="while">
	<div class="container">
		<div class="row">
			<div class="col-md-1">
			<img src="img/ico_partner_1.png">
			</div>
			<div class="col-md-11">			
			Грамотную ценовую политику, разработанную для каждого канала сбыта, гарантирующую прибыль.
			</div>
		</div>
	</div>
</div>

<div class="grey">
	<div class="container">
		<div class="row">
			<div class="col-md-1">
			<img src="img/ico_partner_2.png">
			</div>
			<div class="col-md-11">			
			
				Высокую маржинальность продаж.

			</div>
		</div>
	</div>
</div>

<div class="while">
	<div class="container">
		<div class="row">
			<div class="col-md-1">
			<img src="img/ico_partner_3.png">
			</div>
			<div class="col-md-11">			
			
Обеспечение рекламно-информационными материалами, технической информацией.

			</div>
		</div>
	</div>
</div>

<div class="grey">
	<div class="container">
		<div class="row">
			<div class="col-md-1">
			<img src="img/ico_partner_4.png">
			</div>
			<div class="col-md-11">			
			
				
Помощь индивидуального менеджера в развитии территориальной дистрибуции.


			</div>
		</div>
	</div>
</div>

<div class="while">
	<div class="container">
		<div class="row">
			<div class="col-md-1">
			<img src="img/ico_partner_5.png">
			</div>
			<div class="col-md-11">			
			

Возможность предоставления территориального эксклюзива на специальных условиях.


			</div>
		</div>
	</div>
</div>

<div class="grey">
	<div class="container">
		<div class="row">
			<div class="col-md-1">
			<img src="img/ico_partner_6.png">
			</div>
			<div class="col-md-11">			
			
				

Активную маркетинговую поддержку со стороны Производителя, направленную на увеличение узнаваемости Торговой марки котлов, повышение спроса на продукцию, а значит, и на увеличение объема продаж наших партнеров.



			</div>
		</div>
	</div>
</div>

<div class="while">
	<div class="container">
		<div class="row">
			<div class="col-md-1">
			<img src="img/ico_partner_7.png">
			</div>
			<div class="col-md-11">	

Мотивационные программы для персонала Партнеров, бонусные партнерские программы.

			</div>
		</div>
	</div>
</div>

<div class="grey">
	<div class="container">
		<div class="row">
			<div class="col-md-1">
			<img src="img/ico_partner_8.png">
			</div>
			<div class="col-md-11">		
Конкурентоспособный продукт, который соответствует потребностям рынка.

			</div>
		</div>
	</div>
</div>

<div class="while">
	<div class="container">
		<div class="row">
			<div class="col-md-1">
			<img src="img/ico_partner_9.png">
			</div>
			<div class="col-md-11">	

Возможность финансового участия Производителя в партнерских маркетинговых инициативах.


			</div>
		</div>
	</div>
</div>
<div class="seven_pchin" style="margin-top: 0px">
	<div class="container">
			<div class="seven_content_top">
<svg class="seven_svg_left" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="0,0 100,300 0,300"/>
</svg>
<center>
<h3>
Давайте строить взаимовыгодные отношения и<br> радовать потребителей отличным продуктом!
</h3>
</center>

<svg class="seven_svg_right" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="100,0 0,300 100,300"/>
</svg>

			</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>