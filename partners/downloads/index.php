<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Материалы для скачивания");
?>
<div id="results">
<a name="h1"></a>
<?

$dir    = 'file';
$files = array_diff(scandir($dir), array('..', '.'));
$files = array_chunk($files, 2);

foreach($files as $grup){
?>
<div class="col-xs-12 doc_grup">
<?	
	foreach($grup as $direktoria){
	$info = new SplFileInfo($direktoria);
$Extension = $info->getExtension();
if (!$Extension)
{
$id = uniqid();	
	?>
<div class="col-md-1 col-xs-12 doc_dow_1">
<div class="papka" ><span class="pol1"></span> <span class="pol2"></span> <span class="pol3"></span><span class="pol4"></span> </div>
</div>
<div class="col-md-5 col-xs-12 doc_dow_2">
<a id="dir<?=$id?>" ><?=$direktoria?>
</a>
</div>
<script type="text/javascript">
jQuery('#dir<?=$id?>').click(function(){
$.get(
  "/partners/downloads/ajax.php",
  {
    dir: "<?=$dir?>/<?=$direktoria?>",
  },
  onAjaxSuccess
);
 
function onAjaxSuccess(data)
{  

$('#results').html(data);
						  
}
						
});

</script>
<?
} else {?>
   <div class="col-md-1 col-xs-12 doc_dow_1">
<div class="doc" ><span><?=$Extension?></span> <div class="arrow"></div></div>
</div>
<div class="col-md-5 col-xs-12 doc_dow_2">
<a href="<?=$dir?>/<?=$direktoria?>"><?=$direktoria?></a>
</div>
<?
}

}
?>
</div>
<?

}


?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>