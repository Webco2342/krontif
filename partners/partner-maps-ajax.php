﻿<?
function file_get_contents_from_url($url){

    $data = file_get_contents($url);

    if ($data === false){

        if (function_exists('curl_init')){

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            $data = curl_exec($curl);
            curl_close($curl);

        }

    }

    return $data;

}
$name = $_GET[region]." ".$_GET[cit]." ".$_GET[ul]." ".$_GET[dom]; //GOOGLE API KEY

$response = json_decode(file_get_contents_from_url('https://geocode-maps.yandex.ru/1.x/?geocode='.$name.'&format=json&results=1')); 


  
if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) 
{ 
    $center = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos; 
	$index = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->Address->postal_code;
	$adrese = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->text;
} 
else 
{ 
    echo 'Ничего не найдено'; 
};

$center = explode(" ", $center);

 ?>
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">
    </script>


 <script type="text/javascript">
        ymaps.ready(init);
      
        function init(){ 
            var myMap = new ymaps.Map("map", {
                center: [<?=$center[1]?>, <?=$center[0]?>],
                zoom: 17
            }); 
            
            var myPlacemark = new ymaps.Placemark([<?=$center[1]?>, <?=$center[0]?>], {
                hintContent: '<?=$index?> <?=$adrese?>',
                balloonContent: '<?=$index?> <?=$adrese?>'
            });
            
            myMap.geoObjects.add(myPlacemark);
        }
    </script>
	
	 <div id="map" style="width: 300px; height: 300px"></div>