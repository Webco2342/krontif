<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наши территориальные Дистрибьюторы");
?>			</div>
		</div>
	</div>
</div>



<div class="seven_pchin">
	<div class="container">
			<div class="seven_content_top">
<svg class="seven_svg_left" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="0,0 100,150 0,300"/>
</svg>
<center>
<div class="red_get">
<p><b>Уважаемые потенциальные Дилеры!</b></p>

<p>Мы заинтересованы в развитии Дилерской региональной сети, чтобы Конечные Потребители имели возможность оперативно и по приятной цене приобретать котлы <strong style="color:#e42426;">KRONTIF</strong> в любом регионе России.</p>

 <p>В этом нам содействуют наши Территориальные Дистрибьюторы, к которым Вы можете обращаться по вопросам оптовых закупок котлов.  Кликните по карте на необходимый Федеральный округ и появится список компаний.</p>

 


</div>
</center>

<svg class="seven_svg_right" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="100,0 0,150 100,300"/>
</svg>

			</div>
	</div>
</div>

<div class="while">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
<svg version="1.1"  width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 954 468" preserveAspectRatio="none" >
   <image width="954" xlink:href="map.png" x="0" y="0" height="468"></image>
					<g>
					<a <?if($_GET['OKR']=='1' || $_GET['OKR']==false){?>class="active_okr"<?}?> xlink:href="/partners/get/?OKR=1#table"><text x="100" y="180" fill="#29292d" font-size="18" >
					Центральный 
					<tspan dx="-110" dy="18">
					федеральный округ
					</tspan>				
					</text></a>					
					
					<circle r="3" cx="188" cy="213" fill="#e31e24" stroke="none" stroke-width="0"></circle>
					</g>
					
					<g>
					<a <?if($_GET['OKR']=='2'){?>class="active_okr"<?}?> xlink:href="/partners/get/?OKR=2#table"><text x="250" y="160" fill="#29292d" font-size="18" >
					Северо-Западный
					<tspan dx="-145" dy="18">
					федеральный округ
					</tspan>				
					</text></a>					
					
					<circle r="3" cx="240" cy="155" fill="#e31e24" stroke="none" stroke-width="0"></circle>
					</g>
					
					<g>
					<a <?if($_GET['OKR']=='3'){?>class="active_okr"<?}?> xlink:href="/partners/get/?OKR=3#table">
					<text x="200" y="250" fill="#29292d" font-size="18" >
					Приволжский
					<tspan dx="-110" dy="18">
					федеральный округ
					</tspan>				
					</text></a>					
					
					<circle r="3" cx="250" cy="280" fill="#e31e24" stroke="none" stroke-width="0"></circle>
					</g>
					
					<g>
					<a <?if($_GET['OKR']=='4'){?>class="active_okr"<?}?> xlink:href="/partners/get/?OKR=4#table">
					<text x="60" y="285" fill="#29292d" font-size="18" >
					Южный
					<tspan dx="-65" dy="18">
					федеральный округ
					</tspan>				
					</text></a>					
					
					<circle r="3" cx="135" cy="310" fill="#e31e24" stroke="none" stroke-width="0"></circle>
					</g>
					
					<g>
					<a <?if($_GET['OKR']=='5'){?>class="active_okr"<?}?> xlink:href="/partners/get/?OKR=5#table">
					<text x="135" y="375" fill="#29292d" font-size="18" >
					Северо-Кавказский
					<tspan dx="-155" dy="18">
					федеральный округ
					</tspan>				
					</text></a>					
					
					<circle r="3" cx="125" cy="360" fill="#e31e24" stroke="none" stroke-width="0"></circle>
					</g>
					
					<g>
					<a <?if($_GET['OKR']=='6'){?>class="active_okr"<?}?> xlink:href="/partners/get/?OKR=6#table">
					<text x="395" y="250" fill="#29292d" font-size="18" >
					Уральский
					<tspan dx="-85" dy="18">
					федеральный округ
					</tspan>				
					</text></a>					
					
					<circle r="3" cx="415" cy="280" fill="#e31e24" stroke="none" stroke-width="0"></circle>
					</g>
					
					<g>
					<a <?if($_GET['OKR']=='7'){?>class="active_okr"<?}?> xlink:href="/partners/get/?OKR=7#table">
					<text x="510" y="330" fill="#29292d" font-size="18" >
					Сибирский
					<tspan dx="-90" dy="18">
					федеральный округ
					</tspan>				
					</text></a>					
					
					<circle r="3" cx="535" cy="310" fill="#e31e24" stroke="none" stroke-width="0"></circle>
					</g>
					
					<g>
					<a <?if($_GET['OKR']=='8'){?>class="active_okr"<?}?> xlink:href="/partners/get/?OKR=8#table"><text x="650" y="180" fill="#29292d" font-size="18" >
					Дальневосточный 
					<tspan dx="-145" dy="18">
					федеральный округ
					</tspan>				
					</text></a>					
					
					<circle r="3" cx="688" cy="213" fill="#e31e24" stroke="none" stroke-width="0"></circle>
					</g>

</svg>
			
			
			</div>
			<div class='table-responsive'>
<?if($_GET['OKR']=='1' || $_GET['OKR']==false){?>	
<a name="table"></a>	
<table class="table table-bordered  table-hover" style="vertical-align: inherit; text-align: center;">
<thead class="thead">
<tr>
<th style="text-align: center;">
Области покрытия		
</th>
<th style="text-align: center;">
Название компании			
</th>
<th style="text-align: center;">
Адрес склада			
</th>
<th style="text-align: center;">
Контакты					
</th>
<th style="text-align: center;">
Сайт				
</th>
</tr>
</thead>
<tbody>
<tr>
<td>
Воронежская, Липецкая, Тамбовская, Орловская,<br>  Белгородская, Курская, Саратовская, Рязанская, <br>Пензинская обл, Тверская, Тульская, Костромская, <br>Ярославская, Ивановская
		
</td>
<td>
ТД Блокарт

</td>
<td>
МО, Пятницкое шоссе, 34 км от МКАД, д. Марьино
	
</td>
<td>
8(926) 662-55-76 (опт), 8(903) 221-99-88

</td>
<td>
<a href="http://msk.blokart.su/">www.msk.blokart.su</a>
</td>
</tr>

<tr>
<td>
Воронежская, Липецкая, Тамбовская, Орловская,<br>  Белгородская, Курская, Саратовская, Рязанская,<br> Пензинская обл, Тверская, Тульская, Костромская,<br> Ярославская, Ивановская, Московская

	
</td>
<td>
ТД Блокарт

		
</td>
<td>
МО, Серпуховской район, д. Васильевское, д. 31, "ТД Бегемот"

		
</td>
<td>
7(495) 185-01-02 (опт), 7(495) 796-25-55
		
</td>
<td>
<a href="http://msk.blokart.su/">http://msk.blokart.su/</a>

</td>
</tr>

<tr>
<td>
Тульская, Воронежска, Калужская, Брянская,<br> Смоленская

	
</td>
<td>
ООО "ПрогрессЭнерго"

		
</td>
<td>
Калужская обл. г.Киров, пл.Заводская,2

		
</td>
<td>
8 (48456) 5-59-15, 5-16-69,
сот. 8-910-916-38-99, 8-910-706-49-15


</td>
<td>
<a href="http://www.progressenergo.ru">http://www.progressenergo.ru</a>

</td>
</tr>

<tr>
<td>
Тульская, Воронежска, Калужская, Брянская,<br> Смоленская

	
</td>
<td>
ООО "Климат-Трейд"

		
</td>
<td>
Калуга, ул.Болдина, 67, к.3, офис 322
		
</td>
<td>
(4842) 92-66-76, (495) 668-08-027 (910) 706-49-15

</td>
<td>
<a href="http://www.climatik.su">http://www.climatik.su</a>

</td>
</tr>

<tr>
<td>
Москва и МО

</td>
<td>
ООО "Теплотерм"
	
</td>
<td>
Москва, Волоколамское ш. 81, корп.2

		
</td>
<td>
8 (495) 902-59-81 
8 (495) 490-59-05	
</td>
<td>
<a href="http://www.teploterm.ru">www.teploterm.ru</a>
</td>
</tr>

<tr>
<td>
Москва и МО

</td>
<td>
ООО "Тепломатика"
		
</td>
<td>
Москва, 117545, г.Москва, ул. Дорожная, д.3, корп. 1
		
</td>
<td>
	7(495) 247-55-47
	
</td>
<td>
<a href="http://www..teplomatica.ru">www.teplomatica.ru</a>

</td>
</tr>

<tr>
<td>
Белгородская область
</td>
<td>
ОВК ТЕРМ
		
</td>
<td>
Белгородский р-н, мкр. Таврово-1, ул. Вишневая, 1А
	
</td>
<td>
	7 919 286 63 57
	
</td>
<td>
<a href="https://ovk-term.ru/">www.ovk-term.ru</a>
</td>
</tr>
</tbody>
</table>
<?}?>
<?if($_GET['OKR']=='2'){?>	
<a name="table"></a>	
<table class="table table-bordered table-hover" style="vertical-align: inherit; text-align: center;">
<thead class="thead">
<tr>
<th style="text-align: center;">
Области покрытия		
</th>
<th style="text-align: center;">
Название компании			
</th>
<th style="text-align: center;">
Адрес склада			
</th>
<th style="text-align: center;">
Контакты					
</th>
<th style="text-align: center;">
Сайт				
</th>
</tr>
</thead>
<tbody>
<tr>
<td>
Ленинградская обл., Санкт-Петербург
		
</td>
<td>
ООО "ПК Энергоконтроль"
	
</td>
<td>
Санкт-Петербург, ул. Ржевская, д. 2Б
		
</td>
<td>
8-(812) 245-39-20

</td>
<td>
<a href="http://www2.vgs.ru">www2.vgs.ru</a>
</td>
</tr>

<tr>
<td>
Архангельская обл.
	
</td>
<td>
ООО "Архпромкомплект"
		
</td>
<td>
г. Архангельск, ул. Попова, 15
		
</td>
<td>
тел. (8182)65-52-49	
</td>
<td>

<a href="https://apk.ru">www.apk.ru</a>
</td>
</tr>

<tr>
<td>
Новгородская обл.

</td>
<td>
ИП Ашухин А.А.
		
</td>
<td>
г. Великий Новгород, ул. Нехинская д.57 стр.3
		
</td>
<td>
8 960 205 46 47 (опт, розница)

</td>
<td>

</td>
</tr>

<tr>
<td>
Республика Коми
	
</td>
<td>
ОАО "Кировская коммерческая компания"
		
</td>
<td>
Адрес склада: Кировская область, г.Киров, ул.Северо-Садовая 16а<br>
Адрес шоу-рума: Кировская область, г.Киров, ул.Лепсе 23Б

</td>
<td>
8(8332) 690-420, 690-710,<br> 43-53-10, +7-982-383-53-10


</td>
<td>

<a href="http://www.3kkk.ru/">www.3kkk.ru</a>
</td>
</tr>
</tbody>
</table>
<?}?>
<?if($_GET['OKR']=='3'){?>	
<a name="table"></a>	
<table class="table table-bordered table-hover" style="vertical-align: inherit; text-align: center;">
<thead class="thead">
<tr>
<th style="text-align: center;">
Области покрытия		
</th>
<th style="text-align: center;">
Название компании			
</th>
<th style="text-align: center;">
Адрес склада			
</th>
<th style="text-align: center;">
Контакты					
</th>
<th style="text-align: center;">
Сайт				
</th>
</tr>
</thead>
<tbody>
<tr>
<td>
Кировская обл., Пермский край, <br>Республика Татарстан, Удмуртская республика

</td>
<td>
АО "Кировская коммерческая компания"
</td>
<td>
"Адрес склада: Кировская область, г.Киров, ул.Северо-Садовая 16а
Адрес шоу-рума: Кировская область, г.Киров, ул.Лепсе 23Б"
		
</td>
<td>
8(8332) 690-420, 690-710, 43-53-10, +7-982-383-53-10
</td>
<td>
<a href="http://www.3kkk.ru/">www.3kkk.ru</a>
</td>
</tr>

</tbody>
</table>
<?}?>
<?if($_GET['OKR']=='4'){?>	
<a name="table"></a>	
<center><h3>Вы можете им стать, <a href="http://krontif.beget.tech/partners/usloviya-sotrudnichestva/stat-partnerom.php">заполнив форму «Стать партнером»</a> или позвонив по телефону  8 915 270 53 71</h3></center>
<?}?>
<?if($_GET['OKR']=='5'){?>	
<a name="table"></a>	
<center><h3>Вы можете им стать, <a href="http://krontif.beget.tech/partners/usloviya-sotrudnichestva/stat-partnerom.php">заполнив форму «Стать партнером»</a> или позвонив по телефону  8 915 270 53 71</h3></center>
<?}?>
<?if($_GET['OKR']=='6'){?>	
<a name="table"></a>	
<center><h3>Вы можете им стать, <a href="http://krontif.beget.tech/partners/usloviya-sotrudnichestva/stat-partnerom.php">заполнив форму «Стать партнером»</a> или позвонив по телефону  8 915 270 53 71</h3></center>
<?}?>			
<?if($_GET['OKR']=='7'){?>	
<a name="table"></a>	
<table class="table table-bordered table-hover" style="vertical-align: inherit; text-align: center;">
<thead class="thead">
<tr>
<th style="text-align: center;">
Области покрытия		
</th>
<th style="text-align: center;">
Название компании			
</th>
<th style="text-align: center;">
Адрес склада			
</th>
<th style="text-align: center;">
Контакты					
</th>
<th style="text-align: center;">
Сайт				
</th>
</tr>
</thead>
<tbody>
<tr>
<td>
Иркутская область, <br>
Читинская область, <br>
Республика Саха (Якутия)		
</td>
<td>
ООО “ТеплоТЭН”		
</td>
<td>
г. Иркутск, <br>
ул. Сурнова, 22/7		
</td>
<td>
8-3952-778-351, <br>
778-343, 778-336, <br>
778-103		
</td>
<td>
<a href="http://teploten.ru">www.teploten.ru</a>
</td>
</tr>

</tbody>
</table>
<?}?>
<?if($_GET['OKR']=='8'){?>	
<a name="table"></a>	
<center><h3>Вы можете им стать, <a href="http://krontif.beget.tech/partners/usloviya-sotrudnichestva/stat-partnerom.php">заполнив форму «Стать партнером»</a> или позвонив по телефону  8 915 270 53 71</h3></center>
<?}?>
</div></div>
		</div>
	</div>
</div>

<div class="seven_pchin" style="margin-top:0px">
	<div class="container">
			<div class="seven_content_top" style="height:150px">
<svg class="seven_svg_left" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="0,0 100,300 0,300"/>
</svg>


<svg class="seven_svg_right" version="1.1"  width="90px" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   xml:space="preserve" viewBox="0 0 100 300" preserveAspectRatio="none" >

<polygon fill="#e42426" points="100,0 0,300 100,300"/>
</svg>

			</div>
	</div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>