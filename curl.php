<style>


stop {
  -webkit-animation: colors 12s infinite;
          animation: colors 12s infinite;
}

.stop-2 {
  -webkit-animation-delay: 14s;
          animation-delay: 14s;
}

.stop-3 {
  -webkit-animation-delay: 16s;
          animation-delay: 16s;
}

@-webkit-keyframes colors {
  15% {
    stop-color: transparent;
  }
  30% {
    stop-color: transparent;
  }
  45% {
    stop-color: transparent;
  }
  60% {
    stop-color: transparent;
  }
  75% {
    stop-color: transparent;
  }
  90% {
    stop-color: transparent;
  }
}

@keyframes colors {
  15% {
    stop-color: transparent;
  }
  30% {
    stop-color: transparent;
  }
  45% {
    stop-color: transparent;
  }
  60% {
    stop-color: transparent;
  }
  75% {
    stop-color: transparent;
  }
  90% {
    stop-color: transparent;
  }
}

    </style>
<html>
<body>
<svg viewBox="0 0 1600 200">
<defs>
  <linearGradient id="grad">
    <stop offset="0%" stop-color="crimson" 
          class="stop-1"/>   
    <stop offset="50%" stop-color="gold"
          class="stop-2"/>
    <stop offset="100%" stop-color="teal" 
          class="stop-3"/>
  </linearGradient>
   </defs> 
  
        
		<path fill="url(#grad)" class="tr_el_1" d="M 330 180 L 530 90 L 1300 90 L 1300 110 L 530 110 L 330 180" > </path>
</svg>
</body>
</html>