<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Котлы Кронтиф");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Котлы Кронтиф");
if(CModule::IncludeModule("iblock"))
?>

	<div class="container">
		<div class="row">
			<div class="col-md-12 block">
				<h1 id="preim" class="title-content">9 причин покупать нашу продукцию</h1>	
				
				<div class="col-md-4 con-cart">
					<img src="/local/templates/krontif_new/images/prichina_1.png" width="146" height="146" 
					onmouseover="this.src='/local/templates/krontif_new/images/prichina_1_active.png'; this.width=146;this.height=146;" 
					onmouseout="this.src='/local/templates/krontif_new/images/prichina_1.png'; this.width=146;this.height=146;"
					>
					<h3>Лидерство и опыт</h3>
					<p>
						АО “Кронтиф - Центр”  (”Сукремльский чугунолитейный завод”), относится к числу старейших предприятий России и ведет свою успешную деятельность вот уже 280 лет. Собственная сырьевая база и неограниченные производственные возможности позволяют нам занимать лидирующие позиции на рынке РФ и Европы. 
					</p>
					<strong>Нам можно доверять!</strong>
					
				</div>	

				<div class="col-md-4 con-cart">
					<img src="/local/templates/krontif_new/images/prichina_2.png" width="146" height="146" 
					onmouseover="this.src='/local/templates/krontif_new/images/prichina_2_active.png'; this.width=146;this.height=146;" 
					onmouseout="this.src='/local/templates/krontif_new/images/prichina_2.png'; this.width=146;this.height=146;"
					>
					<h3>КАЧЕСТВО</h3>
					<p>
						Вся продукция в обязательном порядке проходит сертификацию, соответствует требованиям ГОСТ, подвергается лабораторным испытаниям, перед отгрузкой проходит строгий контроль качества.
					</p>
					<strong>Мы дорожим своей репутацией!</strong>

					
				</div>

				<div class="col-md-4 con-cart">
					<img src="/local/templates/krontif_new/images/prichina_3.png" width="146" height="146" 
					onmouseover="this.src='/local/templates/krontif_new/images/prichina_3_active.png'; this.width=146;this.height=146;" 
					onmouseout="this.src='/local/templates/krontif_new/images/prichina_3.png'; this.width=146;this.height=146;"
					>
					<h3>ГАРАНТИЯ ОТ ПРОИЗВОДИТЕЛЯ</h3>
					<p>
						2,5 года гарантии на изделие + 1 год дополнительной гарантии при условии регистрации котла на сайте Производителя. Зарегистрировать можно <a href="/consumers/registration/">тут</a>.
					</p>
					<strong>Мы заботимся о Вашем спокойствии!</strong>

					
				</div>

				<div class="col-md-4 con-cart">
					<img src="/local/templates/krontif_new/images/prichina_4.png" width="146" height="146" 
					onmouseover="this.src='/local/templates/krontif_new/images/prichina_4_active.png'; this.width=146;this.height=146;" 
					onmouseout="this.src='/local/templates/krontif_new/images/prichina_4.png'; this.width=146;this.height=146;"
					>
					<h3>ДОСТУПНОСТЬ ЗАПАСНЫХ ЧАСТЕЙ</h3>
					<p>
						По сравнению с иностранными аналогами запасные части на котлы KRONTIF всегда есть в наличии и доступны по цене.
					</p>
					<strong>Нам важен ваш комфорт!</strong>

					
				</div>

				<div class="col-md-4 con-cart">
					<img src="/local/templates/krontif_new/images/prichina_5.png" width="146" height="146" 
					onmouseover="this.src='/local/templates/krontif_new/images/prichina_5_active.png'; this.width=146;this.height=146;" 
					onmouseout="this.src='/local/templates/krontif_new/images/prichina_5.png'; this.width=146;this.height=146;"
					>
					<h3>ПРОФЕССИОНАЛЬНАЯ ОРГАНИЗАЦИЯ ДОСТАВКИ И МОНТАЖА</h3>
					<p>
						Мы организуем оптимальный способ доставки котла до Вашего адреса, а также предоставим качественные услуги по монтажу, пуско- наладке оборудования или порекомендуем Специализированную организацию.
					</p>
					<strong>Мы не хотим, чтобы Вы переплачивали!</strong>

					
				</div>				
	
				<div class="col-md-4 con-cart">
					<img src="/local/templates/krontif_new/images/prichina_6.png" width="146" height="146" 
					onmouseover="this.src='/local/templates/krontif_new/images/prichina_6_active.png'; this.width=146;this.height=146;" 
					onmouseout="this.src='/local/templates/krontif_new/images/prichina_6.png'; this.width=146;this.height=146;"
					>
					<h3>РАЗУМНАЯ ЦЕНА</h3>
					<p>
						Мы не хотим, чтобы затраты на рекламу увеличивали цену нашего котла ( а так всегда происходит с “раскрученными” товарами — стоимость рекламы оплачивает покупатель). Выбирая наш котел, вы получаетет качественное, долговечное и надежное устройство, которое обеспечит вас теплом и не доставит лишних хлопот. И более ничего!
					</p>
					<strong> Мы знаем, что каждый сэкономленный рубль — это рубль заработанный!</strong>

					
				</div>	

				<div class="col-md-4 con-cart">
					<img src="/local/templates/krontif_new/images/prichina_7.png" width="146" height="146" 
					onmouseover="this.src='/local/templates/krontif_new/images/prichina_7_active.png'; this.width=146;this.height=146;" 
					onmouseout="this.src='/local/templates/krontif_new/images/prichina_7.png'; this.width=146;this.height=146;"
					>
					<h3>АКЦИИ И ПОДАРКИ</h3>
					<p>
						Мы и наши Дилеры любим разыгрывать призы и дарить подарки нашим покупателям. Информация об акциях можно найти на нашем сайте, на сайтах наших партнеров и в розничных магазинах.
					</p>
					<strong> Нам приятно радовать Вас!</strong>

					
				</div>		
				
				<div class="col-md-4 con-cart">
					<img src="/local/templates/krontif_new/images/prichina_8.png" width="146" height="146" 
					onmouseover="this.src='/local/templates/krontif_new/images/prichina_8_active.png'; this.width=146;this.height=146;" 
					onmouseout="this.src='/local/templates/krontif_new/images/prichina_8.png'; this.width=146;this.height=146;"
					>
					<h3>БЕСПЛАТНАЯ ГОРЯЧАЯ ЛИНИЯ <br> 8-800-550-28-20</h3>
					<p>
						Не можетет  выбрать товар? Возникли технические вопросы? Наступил гарантийный случай? Звоните.
					</p>
					<strong> Мы поможем!</strong>

					
				</div>	

				<div class="col-md-4 con-cart">
					<img src="/local/templates/krontif_new/images/prichina_9.png" width="146" height="146" 
					onmouseover="this.src='/local/templates/krontif_new/images/prichina_9_active.png'; this.width=146;this.height=146;" 
					onmouseout="this.src='/local/templates/krontif_new/images/prichina_9.png'; this.width=146;this.height=146;"
					>
					<h3>СКИДКИ ЛЬГОТНЫМ КАТЕГОРИЯМ ГРАЖДАН</h3>
					<p>
						Мы и наши Дилеры гарантированно предоставляем специальные цены для льготных категорий грааждан.
					</p>
					<strong> Мы социально отвественная компания!</strong>
					
				</div>	
				
			</div>			
		</div>
	</div>

	
	<div class="bg_catalog_index">
		<div class="container">
			<div class="row">
				<div class="col-md-12 block">
					<h1 id="preim" class="title-content">котлы  <span>krontif</span></h1>	
					<div class="col-md-1 cart-list"></div>
					<div class="col-md-5 cart-list">
					<a class="index_raz" href="/catalog/series-demidov/">
						<div class="thumbnail">
							<h3>
										Серия «<b>ДЕМИДОВЪ</b>» 15-49 кВт
							</h3>
							<b class="b">Идеальные дровяные котлы для дома и дачи!  </b>
							<img src="upload/image1.png" alt="Котел KRONTIF – 06Т">
							 <div class="caption">

									<ul>
										<li>
											Большая загрузочная камера
										</li>
										<li>
											Адаптирован под установку ТЭНа											
										</li>
										<li>
											Высокое качество чугунного теплообменника										
										</li>
										<li>
											Высокая коррозионная стойкость по сравнению со остальными аналогами										
										</li>
									</ul>
						
							</div>
						</div>
						</a>
					  </div>
					<div class="col-md-5 cart-list">
					<a class="index_raz" href="/catalog/series-siberia/">
						<div class="thumbnail">
							<h3>
										Серия «<b>СИБИРЬ</b>» 21-80кВт
							</h3>
							<b class="b">Угольные котлы, бросающие вызов даже сибирским морозам!  </b>
							<img src="upload/image2.png" alt="Котел KRONTIF – 06Т">
							 <div class="caption">

									<ul>
										<li>
											Срок эксплуатации 25 лет									
										</li>
										<li>
											Неприхотливость и всеядность											
										</li>
										<li>
											Адаптирован под установку газовой горелки											
										</li>
										<li>
											КПД до 83%											
										</li>
									</ul>
						
							</div>
						</div>
					</a>	
					  </div>	
					 
				</div>	
					<div class="col-md-1 cart-list"></div>
					  <div class="col-md-11 hit_title">
						<img src="/local/templates/krontif_new/images/hit.png" alt=""><h3>Хиты продаж</h3>
					   </div>				
			</div>	
			
		</div>		
	</div>	
	<div class="bg_catalog_index_hit">
		<div class="container">
			<div class="row">	
				<div class="col-md-2"></div>
				<div class="col-md-8">
				<?
					$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*");
					$arFilter = Array("IBLOCK_ID"=>7, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_HIT"=>TRUE);
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);
					while($ob = $res->GetNextElement())
					{
					$arFields = $ob->GetFields();
					$arProps = $ob->GetProperties();	
						?>
				<div class="col-md-3 hit_list">
				<span class="hit"><img src="/local/templates/krontif_new/images/hit_cart.png"></span>
				<a href="/catalog/detail.php?id=<?=$arFields["ID"]?>">
					<img src="<?= CFile::GetPath($arFields["PREVIEW_PICTURE"])?>" alt="<?=$arFields["NAME"]?>">
					<div>
					
						<?=$arFields["NAME"]?>
					
					</div>
				</a>
				</div>
	<?

}
?>			

				

				</div>
			</div>
		</div>
	</div>
	<div class="bg_catalog_index_hit_clic">	
		<div class="container">
			<div class="row ">	
				<div class="col-md-1"></div>
					<div class="col-md-10 catalog_index_hit_clic">
						<div class="form-group right">
						<a href="/catalog" type="submit" class="btn_form">
						<svg class="pie_form" version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">		
						<defs>
							<linearGradient id="gradient">
								<stop offset="0%" style="stop-color: #ed1c24"></stop>
								<stop offset="100%" style="stop-color: #97080e"></stop>
							</linearGradient>
						</defs>
					<path class="tr_el_1" d="M 0 45 L 335 0 L 100 155  z"> </path>
					<path class="tr_el_2" fill="url(#gradient)" d="M 60 25 L 250 25 L 250 107 L 32 107  z"> </path>
					<text x="71" y="75" fill="#fff" font-size="30">ВСЕ КОТЛЫ </text>
					</svg>	
						
						</a>
					</div>
					</div>
			</div>
		</div>
	</div>	
	
	<div class="container">
		<div class="row">
			<div class="col-md-12 block">
				<h1 class="title-content prem_titl">НАШИ ПРЕИМУЩЕСТВА</h1>
				
		<div class="container_diogramm">	
				

<svg class="pie" version="1.2" viewBox="0 0 1062 1025">

<g class="svg_g">        
<path class="one red" d="M 120 100 A 345,345 1 0 1 355,0 L 355 335  z"> </path>
<text x="275" y="55" class="svg_text" font-size="16" font-weight="900">Высокая 
<tspan dx="-150" dy="20">ремонтопригодность </tspan>
<tspan dx="-103" dy="20">по сравнению </tspan>
<tspan dx="-178" dy="20">со стальными аналогами</tspan>
</text> 
<path class="white" d="M 220 200 A 215,215 1 0 1 355,145 L 355 335  z"> </path>
</g>

<g class="svg_g"> 
<path class="two red" d="M 370 0 A 345,345 1 0 1 605,100 L 370 335  z">  </path>
<text x="380" y="55" class="svg_text" font-size="16" font-weight="900">
Высокая 
<tspan x="380" dy="20">
гидравлическая прочность 
</tspan>
<tspan x="380" dy="20">
литого изделия по сравнению 
</tspan>
<tspan x="380" dy="20">
с конструкциями со 
</tspan>
<tspan x="380" dy="20">
сварными швами
</tspan>
</text> 
<path class="white" d="M 370 145 A 215,215 1 0 1 505,200 L 370 335  z">  </path>
</g>

<g class="svg_g">  
<path class="three red" d="M 615 110 A 345,345 1 0 1 715,345 L 380 345  z">  </path>
<text x="585" y="225" class="svg_text" font-size="16" font-weight="900">Высокая 
<tspan x="585" dy="20">коррозионная </tspan>
<tspan x="585" dy="20">стойкость</tspan>

</text> 
<path class="white" d="M 515 210 A 215,215 1 0 1 570,345 L 380 345  z">  </path>
</g>

<g class="svg_g">  
<path class="four red" d="M 715 360 A 345,345 1 0 1 615,595 L 380 360  z">  </path>
<text x="565" y="450" class="svg_text" font-size="16" font-weight="900">Цены "Эконом" -
<tspan x="565" dy="20">качество </tspan> 
<tspan x="565" dy="20">"Премиум" </tspan>
</text>
<path class="white" d="M 570 360 A 215,215 1 0 1 515,495 L 380 360  z">  </path>
</g>

<g class="svg_g">  
<path class="five red" d="M 605 605 A 345,345 1 0 1 370,705 L 370 370  z">  </path>
<text x="380" y="580" class="svg_text" font-size="16" font-weight="900">
Качественный чугун
<tspan x="380" dy="20">собственного </tspan>
<tspan x="380" dy="20">производства,</tspan>
<tspan x="380" dy="20">высокое качество </tspan>
<tspan x="380" dy="20">литья </tspan>
</text>
<path class="white" d="M 505 505 A 215,215 1 0 1 370,560 L 370 370  z">  </path>
</g>

<g class="svg_g">  
<path class="six red" d="M 355 705 A 345,345 1 0 1 120,605 L 355 370  z">  </path>
<text x="220" y="580" class="svg_text" font-size="16" font-weight="900">
Можно нарастить 
<tspan dx="-174" dy="20">
мощность котла, добавив 
</tspan>
<tspan dx="-184" dy="20">
секцию, если увеличилась 
</tspan>
<tspan dx="-174" dy="20">
площадь отапливаемого 
</tspan>
<tspan dx="-85" dy="20">
помещения
</tspan>
</text>
<path class="white" d="M 355 560 A 215,215 1 0 1 220,505 L 355 370  z">  </path>
</g>

<g class="svg_g">  
<path class="seven red" d="M 110 595 A 345,345 1 0 1 10,360 L 345 360  z">  </path>
<text x="50" y="395" class="svg_text" font-size="16" font-weight="900">
Длительный 
<tspan x="50" dy="20">
срок 
</tspan>
<tspan x="50" dy="20">
эксплуатации - 
</tspan>
<tspan x="50" dy="70">
от 
</tspan>
<tspan dx="10" dy="0" font-size="75">
25 
</tspan>
<tspan dx="-10" dy="0">
лет
</tspan>
</text>
<path class="white" d="M 210 495 A 215,215 1 0 1 155,360 L 345 360  z">  </path>
</g>

<g class="svg_g">  
<path class="eight red" d="M 10 345 A 345,345 1 0 1 110,110 L 345 345  z">  </path>
<text x="55" y="225" class="svg_text" font-size="16" font-weight="900">
Простое 
<tspan x="55" dy="20">
обслуживание 
</tspan>
<tspan x="55" dy="20">
и уход
</tspan>
</text>
<path class="white" d="M 155 345 A 215,215 1 0 1 210,210 L 345 345  z">  </path>
</g>
<g class="svg_g_center"> 
<path class="center_svg" d="M235,225 a 180,180 0 1 0  1-1z" /> 

<path class="white" d="M 285 189 A 180,180 1 0 1 300,182 L 300 205  z">  </path>
<path class="white" d="M 425 182 A 180,180 1 0 1 440,189 L 425 205  z">  </path>
<path class="white" d="M 530 280 A 180,180 1 0 1 535,295 L 510 295  z">  </path>
<path class="white" d="M 535 415 A 180,180 1 0 1 528,430 L 510 415  z">  </path>
<path class="white" d="M 435 517 A 180,180 1 0 1 420,525 L 420 500  z">  </path>
<path class="white" d="M 305 525 A 180,180 1 0 1 285,525 L 305,500  z">  </path>
<path class="white" d="M 195 425 A 180,180 1 0 1 190,410 L 215 410  z">  </path>
<path class="white" d="M 190 295 A 180,180 1 0 1 175,260 L 215 295  z">  </path>



<image xlink:href="/local/templates/krontif_new/images/Layer 96.png" x="265" y="220" width="196"  />

</g>

<path class="ten" d="M 240 700 A 348,365 0 1 0 625,110 A 363,363 1 0 1 240,700  z">  </path>
</svg>	
				</div>	
			</div>
		</div>
	</div>
	
	<div class="bg_catalog_index_krugi">
		<div class="container ">
			<div class="row">
				<div class="col-md-12 block">
					<h1 class="title-content prem_titl">принцип устройства котла</h1>	
					<svg width="100%" height="100%" class="pie_kotel" version="1.2" viewBox="0 0 980 870" preserveAspectRatio="xMidYMid meet">
					
					<image width="270" xlink:href="/local/templates/krontif_new/images/kotel.png" x="365" y="125" width="314" height="627" />
					<g>
					
					<a xlink:href="/catalog/spare-parts/"><text x="550" y="100" fill="#29292d" font-size="20" font-weight="100">Терморегулятор </text></a>
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="520,185 520,110  620,110" />
					<circle r="5" cx="625" cy="110"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />						
					</g>
					
					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="700" y="150" fill="#29292d" font-size="20" font-weight="100">Верхняя панель кожуха </text></a>
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="580,242 620,160  720,160" />
					<circle r="5" cx="725" cy="160"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>
					
					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="730" y="230" fill="#29292d" font-size="20" font-weight="100">Дымовой отражатель </text></a>
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="560,342 730,240  795,240" />
					<circle r="5" cx="800" cy="240"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />					
					</g>	

					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="750" y="290" fill="#29292d" font-size="20" font-weight="100">лючок 
					<tspan dx="-60" dy="20">
					вторичного воздуха
					</tspan></a>
					</text>
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="505,410 780,320  865,320" />
					<circle r="5" cx="870" cy="320"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>	
					
					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="770" y="390" fill="#29292d" font-size="20" font-weight="100">дверца загрузки</text></a>
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="580,450 800,400  895,400" />
					<circle r="5" cx="900" cy="400"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />				
					</g>	
					
					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="770" y="490" fill="#29292d" font-size="20" font-weight="100">правая 
					<tspan dx="-65" dy="20">
					панель кожуха
					</tspan>
					</text></a>
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="615,500 800,520  885,520" />
					<circle r="5" cx="890" cy="520"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />					
					</g>
					
					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="770" y="590" fill="#29292d" font-size="20" font-weight="100">прижим решетки 
					<tspan dx="-155" dy="20">
					шуровочной
					</tspan>
					</text></a>
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="570,545 750,620  855,620" />
					<circle r="5" cx="860" cy="620"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />					
					</g>
					
					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="700" y="710" fill="#29292d" font-size="20" font-weight="100">
					решетка шуровочная 					
					</text>	</a>				
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="535,555 670,720  755,720" />
					<circle r="5" cx="760" cy="720"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>
					
					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="600" y="810" fill="#29292d" font-size="20" font-weight="100">
					ручка регулировки открытия 
					<tspan dx="-258" dy="20">
					поддувальной дверцы		
					</tspan>					
					</text>	</a>				
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="495,660 580,790  640,790" />
					<circle r="5" cx="645" cy="790"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>

					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="200" y="820" fill="#29292d" font-size="20" font-weight="100">
					дверца поддувальная				
					</text>	</a>				
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="475,680 415,800  340,800" />
					<circle r="5" cx="345" cy="800"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>

					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="50" y="710" fill="#29292d" font-size="20" font-weight="100">
					ручка открытия дверцы 
					<tspan dx="-211" dy="20">
					шуровочной	
					</tspan>					
					</text>	</a>				
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="385,610 205,690  145,690" />
					<circle r="5" cx="150" cy="690"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>
					
					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="50" y="640" fill="#29292d" font-size="20" font-weight="100">
					дверца шуровочная				
					</text>	</a>				
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="435,550 155,620  105,620" />
					<circle r="5" cx="110" cy="620"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>	

					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="40" y="530" fill="#29292d" font-size="20" font-weight="100">
					пакет секций			
					</text>	</a>				
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="395,520 155,540  85,540" />
					<circle r="5" cx="90" cy="540"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>	

					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="50" y="370" fill="#29292d" font-size="20" font-weight="100">
					ручка открытия 
					<tspan dx="-140" dy="20">
					дверцы загрузки			
					</tspan>	
					</text>	</a>				
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="385,420 155,400  95,400" />
					<circle r="5" cx="100" cy="400"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>	

					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="50" y="270" fill="#29292d" font-size="20" font-weight="100">
					левая панель кожуха
					</text>	</a>				
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="375,320 210,280  125,280" />
					<circle r="5" cx="130" cy="280"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>

					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="75" y="200" fill="#29292d" font-size="20" font-weight="100">
					передняя панель кожуха
					</text>	</a>				
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="395,265 250,210  125,210" />
					<circle r="5" cx="130" cy="210"  fill="#e31e24" stroke="#e31e24" stroke-width="0" />
					</g>	

					<g>
					<a xlink:href="/catalog/spare-parts/"><text x="225" y="100" fill="#29292d" font-size="20" font-weight="100">
					термометр
					</text></a>					
					<polyline fill="none" stroke-dasharray="3" stroke="#e31e24" stroke-width="2px" points="410,230 350,110  260,110" />
					<circle r="5" cx="265" cy="110"  fill="#e31e24" stroke="none" stroke-width="0" />
					</g>					
					
					</svg>
				</div>
			</div>
		</div>				
	</div>
	
	<div class="red_title_bg">
		<div class="container ">
			<div class="row">
				<div class="col-md-12 block">
					
				<h1 id="preim" class="title-content_2">калькулятор  <span>мощности</span></h1>	
				
				</div>
			</div>
		</div>				
	</div>
		<div class="bg_catalog_index_hit_clic">
		<div class="container ">
			<div class="row">
				<div class="col-md-12 block center">
					<svg class="pie_tr" version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">		
					<path class="tr_el" d="M 0 0 L 110 0 L 55 55  z"> </path>
					</svg>	 
				</div>
			
				<div class="col-md-12">
<script language="JavaScript" type="text/JavaScript">




 <!--
 function calcTeplopoter()
 { 
 var itogValue = 0.1 * fm_clc.s1.value * fm_clc.s2.value * fm_clc.s3.value * fm_clc.s4.value * fm_clc.s5.value * fm_clc.s6.value * fm_clc.s7.value * 1.0 * fm_clc.s9.value ;
 fm_clc.teplopoter.value = itogValue.toFixed(2);
 var itogPoter = (itogValue * 1.2) -  fm_clc.teplopoter.value;
 fm_clc.poter.value = itogPoter.toFixed(2);

 fm_clc.itog_moshn.value = (itogValue * 1.2).toFixed(2);
 }
 //-->

 </script>
 

				<form class="form-horizontal" role="form" method="POST" name="fm_clc" action="" id="calcform" >
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Площадь помещения:</div>
						<input type="text" name="s9" id="s9" class="col-xs-1" value="100" required=""> <div class="col-xs-1"> м<sup>2</sup></div>		
						<div class="col-xs-1"></div><div class="col-xs-3 form_title">Мощность котла без учета теплопотерь:</div>
					</div>
					
					 
					
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Высота помещения:</div>
						<select class="col-xs-2" name="s7" id="s7">
						  <option value="1.00">2,5</option>
						  <option value="1.05" selected="selected">3,0</option>
						  <option value="1.10">3,5</option>
						  <option value="1.15">4,0</option>
						  <option value="1.20">4,5</option>
						</select>
						<div class="col-xs-1"></div><input id="teplopoter" size="15" value="0" class="col-xs-2 form_result" name="teplopoter" /> <div class="col-xs-1">кВт.</div>
						
					</div>
					
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Стены:</div>
					
						<select class="col-xs-2" name="s2" id="s2">
						  <option value="0.85" selected="selected">Хорошая изоляция</option>
						  <option value="1.0">Ж/бетон, кирпич (2), утеплитель (150 мм)</option>
						  <option value="1.27">Плохая изоляция</option>
						</select>
						<div class="col-xs-1"></div><div class="col-xs-3 form_title">Теплопотери: </div>
					</div>
					<div class="form-group input">	<div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Соотношение площадей окон и пола:</div>
					
						<select class="col-xs-2" name="s3" id="s3">
						  <option value="0.80" selected="selected">10%</option>
						  <option value="0.90">11%-19%</option>
						  <option value="1.00">20%</option>
						  <option value="1.10">21%-29%</option>
						  <option value="1.20">30%</option>
						  <option value="1.30">31%-39</option>
						  <option value="1.40">40%</option>
						  <option value="1,50">50%</option>
						</select>						
						<div class="col-xs-1"></div><input id="poter" name="poter" value="0" class="col-xs-2 form_result" size="15" /> <div class="col-xs-1">кВт.</div>
					</div>
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Тип окон:</div>
					
						<select id="s1" class="col-xs-2" name="s1">
						  <option value="0.85">Тройной стеклопакет</option>
						  <option value="1.0">Двойной стеклопакет</option>
						  <option value="1.27" selected="selected">Обычное (двойное) остекление</option>
						</select>
						<div class="col-xs-1"></div><div class="col-xs-3 form_title">Необходимая мощность котла с учетом теплопотерь: </div>
					</div>
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Число стен, выходящих наружу:</div>
					
						<select id="s5" class="col-xs-2" name="s5">
						  <option value="1.00">Одна</option>
						  <option value="1.11">Две</option>
						  <option value="1.22">Три</option>
						  <option value="1.33" selected="selected">Четыре</option>
						</select>
						<div class="col-xs-1"></div><input id="itog_moshn" name="itog_moshn" value="0" class="col-xs-2 form_result" size="15" /> <div class="col-xs-1">кВт.</div>
					</div>
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Температура снаружи помещения:</div>
					
						<select class="col-xs-2" name="s4" id="s4">
						  <option value="0.70">до -10</option>
						  <option value="0.80" selected="selected">-10</option>
						  <option value="0.90">-15</option>
						  <option value="1.00">-20</option>
						  <option value="1.10">-25</option>
						  <option value="1.20">-30</option>
						  <option value="1.30">-35</option>
						</select>
					</div>	
					
					<div class="form-group input"><div class="col-xs-2"></div>
					<div class="col-xs-3 form_title">Тип помещения над расчитываемым:</div>
					
						<select class="col-xs-2" name="s6" id="s6">
						  <option value="0.82">Обогреваемое помещение</option>
						  <option value="0.91" selected="selected">Теплый чердак</option>
						  <option value="1.00">Холодный чердак</option>
						</select>
					</div>
					
					<div class="form-group right">
						<div id="btncalc" onclick="return calcTeplopoter()" >
						<svg class="pie_form" version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">		
						<defs>
							<linearGradient id="gradient">
								<stop offset="0%" style="stop-color: #ed1c24"></stop>
								<stop offset="100%" style="stop-color: #97080e"></stop>
							</linearGradient>
						</defs>
					<path class="tr_el_1" d="M 0 45 L 335 0 L 100 155  z"> </path>
					<path class="tr_el_2" fill="url(#gradient)" d="M 60 25 L 250 25 L 250 107 L 32 107  z"> </path>
					<text x="71" y="75" fill="#fff" font-size="30">РАССЧИТАТЬ </text>
					</svg>	
						
						</div>
					</div>
					

				</form>		
			</div>
				
			</div>
		</div>				

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>