<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($APPLICATION->GetCurPage(false) !== '/'):?>
</div>
	</div>
		</div>
			</div>
<?endif?>

	</div>	

	<div class="bg_zavod">
	
			<div class="container ">
				<div class="row">
<? if ($APPLICATION->GetCurPage(false) === '/'): ?>					
					<h1 id="preim" class="title-content">отзывы</h1>
	<div id="w">				
<nav class="slidernav">
      <div id="navbtns" class="clearfix">
        <a href="#" class="previous">
		<svg class="pie_nav" version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">		
					<path class="tr_el" d="M 0 55 L 55 0 L 55 110  z"> </path>
		</svg>	
		</a>
        <a href="#" class="next">
		
		<svg class="pie_nav" version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">		
					<path class="tr_el" d="M 55 55 L 0 0 L 0 110  z"> </path>
		</svg>	
		
		</a>
      </div>
    </nav>
    
    <div class="crsl-items" data-navigation="navbtns">
      <div class="crsl-wrap">
	  <?
	  if(CModule::IncludeModule("iblock"))
		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT","PROPERTY_*");
		$arFilter = Array("IBLOCK_ID"=>9, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
		while($ob = $res->GetNextElement())
		{
		 $arFields = $ob->GetFields();
		 $arProps = $ob->GetProperties();
		 $text = mb_strimwidth($arFields["PREVIEW_TEXT"], 0, 200, "...");
	  ?>
        <div class="crsl-item css_popup<?=$arFields["ID"]?>">         
          <p><?=$text?></p>         
		  
		<svg class="pie_readmore" version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">		
					<path class="red" d="M 380 0 L 350 30 L 380 125  z"> </path>
					<path class="tr_el" d="M 0 125 L 380 125 L 380 60  z"> </path>
					
		</svg>		  
		  
        </div><!-- post #1 -->
		<div id="hideBlock<?=$arFields["ID"]?>" style="display:none; max-width:850px">
		
		   <h3><?=$arFields["NAME"]?> </h3>
		 
		 <p >
		<?=$arFields["PREVIEW_TEXT"]?>
		</p>
		</div>
		<script type="text/javascript" language="javascript">
		   window.BXDEBUG = true;
		BX.ready(function(){
		   var oPopup = new BX.PopupWindow('call_feedback', window.body, {
			  autoHide : true,
			  offsetTop : 1,
			  offsetLeft : 0,
			  lightShadow : true,
			  closeIcon : true,
			  closeByEsc : true,
			  overlay: {
			   
			  }
		   });
		   oPopup.setContent(BX('hideBlock<?=$arFields["ID"]?>'));
		   BX.bindDelegate(
			  document.body, 'click', {className: 'css_popup<?=$arFields["ID"]?>' },
				 BX.proxy(function(e){
					if(!e)
					   e = window.event;
					oPopup.show();
					return BX.PreventDefault(e);
				 }, oPopup)
		   );
		   
		   
		});
		</script>
        <?}?>


        
      

      </div><!-- @end .crsl-wrap -->
    </div><!-- @end .crsl-items -->
	</div>					
<?endif?>	
<? if ($APPLICATION->GetCurPage(false) !== '/'): ?>	
<svg  width="100%" height="500" class="" viewBox="0 0 1024 500" preserveAspectRatio="xMidYMid meet">
	
	<text x="150" y="250" class="molot red" font-size="82">
	НА РЫНКЕ
	<tspan class="comp" fill="#000000" dx="-300" dy="52" font-size="50" font-weight="100" >
	чугунного литья
	</tspan>
	<tspan class="molot" fill="#000000" dx="10" dy="-2" font-size="189" >
	280
	</tspan>	
	<tspan class="molot" fill="#000000" dx="-200" dy="65" font-size="61" >
	ЛЕТ
	</tspan>	
	</text>
	
</svg>

<?endif?>				
				</div>
			</div>
		
		<svg class="promo_index_footor" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	


<line fill="none" stroke="#fff" x1="0" stroke-width="6px" y1="50%" x2="100%" y2="50%"/>	
	
<svg  width="100%" height="100%" y="50" class="svg_g_footer" viewBox="0 0 740 330" preserveAspectRatio="xMidYMid meet">
<g>
<polyline fill="#1f1f1f" stroke="#fff" stroke-width="6px" points="5,65 105,5 205,65 205,165 105,225 5,165 5,63" />

<image xlink:href="/local/templates/krontif_new/images/ico_zavod.png" x="62" y="40" width="89" height="55" />
<text x="75" y="120" class="svg_text" font-size="20" font-weight="100">
СЫРЬЕ  
<tspan dx="-100" dy="25">
СОБСТВЕННОГО
</tspan>
<tspan dx="-145" dy="25">
ПРОИЗВОДСТВА
</tspan>
</text> 
</g>

<g>
<polyline fill="#1f1f1f" stroke="#fff" stroke-width="6px" points="270,65 370,5 470,65 470,165 370,225 270,165 270,63" />


<text x="308" y="110" class="svg_red" fill="#ed1c24" font-size="30" font-weight="900">
ВЫСОКОЕ
<tspan fill="#fff" font-size="20" dx="-145" dy="35">
КАЧЕСТВО ЛИТЬЯ
</tspan>

</text> 
</g>

<g>
<polyline fill="#1f1f1f" stroke="#fff" stroke-width="6px" points="535,65 635,5 735,65 735,165 635,225 535,165 535,63" />

<text x="565" y="95" class="svg_red" fill="#ed1c24" font-size="30" font-weight="900">
ЧУГУННАЯ
<tspan fill="#fff" font-size="20" dx="-120" dy="30">КЛАССИКА</tspan>
<tspan fill="#fff" font-size="20" dx="-125" dy="25" font-weight="100">
ПО ДОСТУПНОЙ
</tspan>
<tspan fill="#fff" font-size="20" dx="-100" dy="25" font-weight="100">
ЦЕНЕ
</tspan>
</text> 
</g>
</svg>
		</svg>		
		
	</div>
	<div class="maps_index">
<div id="map-canvas"></div>
<div class="bs-example">
    <div class="alert alert-warning fade in">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <strong>Ирина Прохорова</strong> 
	  <div class="p"><span class="glyphicon glyphicon-phone"></span> <div> +7 495 725 56 82 (доб. 321)<br>
		+7 915 270 53 71
		</div></div>
		<div class="p"><span class="glyphicon glyphicon-envelope"></span><div>
		prohorova@metholding.com</div>
		</div>
    </div>
  </div>
  </div>


<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-5">
			<img src="/local/templates/krontif_new/images/logo_bootom.png" alt="Котел KRONTIF – 06Т">
			
			<div class="footer_cont">
				© 2018 АО «КРОНТИФ-ЦЕНТР»<br>
				249401, Россия, Калужская область,<br>
				г. Людиново, ул. Щербакова, 1а
				
				<br><br>
				115419, Россия, г. Москва,<br> 2-ой Верхний Михайловский пр-д, д.9 
				
				
			</div>
			
			</div>
			<div class="col-md-3">
			
			</div>
			<div class="col-md-3">
			<?$APPLICATION->IncludeComponent("bitrix:search.suggest.input", "template1", Array(
				"DROPDOWN_SIZE" => "10",	// Размер выпадающего списка
					"INPUT_SIZE" => "20",	// Размер поля ввода
					"NAME" => "q",	// Имя поля ввода
					"VALUE" => "",	// Содержимое поля ввода
				),
				false
			);?>
			</div>
			<div class="col-md-7">
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu", 
				"bottom_menu", 
				array(
					"ROOT_MENU_TYPE" => "top",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "36000000",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_THEME" => "site",
					"CACHE_SELECTED_ITEMS" => "N",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "left",
					"USE_EXT" => "Y",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"COMPONENT_TEMPLATE" => "horizontal"
				),
				false
			);?>
			</div>
		</div>
	</div>

</footer>
<!---
<?if($_COOKIE['BITRIX_SM_BANNER_KRONTIF_LEFT'] == false && $APPLICATION->GetCurPage(false) !== '/consumers/salle/'){?><?}?>
<div id='fixedbanner'>
<a class="close" id="close" onclick='var el=document.getElementById("fixedbanner"); el.parentNode.removeChild(el);' style='right: 10px;position: absolute;top: 5px;z-index: 99;color:#fff;opacity: .8;'>×</a>
			<svg width="100%" height="100%" class="pie_banner" version="1.2" viewBox="0 0 800 375" preserveAspectRatio="xMidYMid meet">
			<image class="banner_1" xlink:href="/local/templates/krontif_new/images/bg_banner_4_center.png" width="800" height="375" />
		  <svg width="100%" height="100%" viewBox="0 0 800 375" preserveAspectRatio="xMidYMid meet">
		  <image xlink:href="/local/templates/krontif_new/images/bg_banner_4_center_vn.png" width="650" height="343" />
		  <text class="molot" x="150"  y="50" fill="#37373b" font-size="42px" font-weight="100">
			ВНИМАНИЕ!
			<tspan dx="-310" dy="45" class="comp" fill="#37373b" font-size="35px">
			ОБЪЯВЛЯЕТСЯ 
			</tspan>
			<tspan dx="0" dy="0" class="molot" fill="#37373b" font-size="45px">
			KRONTIF 
			</tspan>
			<tspan dx="-25" dy="0" class="comp" fill="#37373b" font-size="30px">
			O 
			</tspan>
			<tspan dx="-2" dy="0" class="comp" fill="#ed1c24" font-size="30px">
			МАНИЯ
			</tspan>
			<tspan dx="0" dy="0" class="molot" fill="#37373b" font-size="70px">
			!
			</tspan>
		  </text>
		  
		  <text class="comp" x="85"  y="160" fill="#ed1c24" font-size="40px" font-weight="300">
			УСПЕЙ
			<tspan dx="0" dy="0" class="comp" fill="#ed1c24" font-size="30px">
			купить 
			</tspan>
			<tspan dx="0" dy="0" class="molot" fill="#ed1c24" font-size="35px">
			КОТЕЛ
			</tspan>
			<tspan dx="-5" dy="0" class="comp" fill="#37373b" font-size="30px">
			по
			</tspan>
			<tspan dx="-2" dy="0" class="molot" fill="#37373b" font-size="40px">
			ВКУСНОЙ 
			</tspan>
			<tspan dx="0" dy="0" class="comp" fill="#37373b" font-size="30px">
			цене
			</tspan>
			<tspan dx="-500" dy="40" class="comp" fill="#37373b" font-size="30px">
			и получить
			</tspan>
			<tspan dx="-50" dy="40" class="molot" fill="#37373b" font-size="30px">
			ПОЛЕЗНЫЙ
			</tspan>
			<tspan dx="-0" dy="0" class="comp" fill="#37373b" font-size="30px">
			премиум-
			</tspan>
			<tspan dx="-5" dy="0" class="molot" fill="#37373b" font-size="30px">
			ПОДАРОК!
			</tspan>
		  </text>

		  </svg>
		   <image x="505"  y="201" xlink:href="/local/templates/krontif_new/images/banner_u_r_b.png" width="400" height="175" />
		   <image x="585"  y="165" xlink:href="/local/templates/krontif_new/images/stels.png" width="205" height="205" />
		   <image x="530"  y="175" xlink:href="/local/templates/krontif_new/images/lenta.png" width="304" height="218" />
		  <text class="comp" x="150"  y="350" fill="#fff" font-size="30px" font-weight="100">
		  <a fill="#fff" class="a_svg" xlink:href="/consumers/salle/"> Хочу купить</a>
		  	<tspan dx="150" dy="0" class="comp" >
			<a fill="#fff" class="a_svg" onclick='var el=document.getElementById("fixedbanner"); el.parentNode.removeChild(el);' > Не интересно</a>
			</tspan>
		 </text>
		   
			</svg>	
</div>
--->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.transit/0.9.9/jquery.transit.min.js'></script>

		<script type="text/javascript" src="/local/templates/krontif/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="/local/templates/krontif/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="/local/templates/krontif/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript" src="http://bootstrap-ru.com/203/assets/js/bootstrap-modal.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
			
			jQuery('#close').click(function(){ 
			var objTest = { 
   test    : 5,  
   bla     : true , 
   some : false
}   
				BX.ajax({
						   url: '/cookie-ajax.php',
						   data: objTest, 
							
				});
			});
		</script>	
	    <script type="text/javascript" src="/local/templates/krontif_new/js/responsiveCarousel.min.js"></script>	
		
		<script type="text/javascript">
$(function(){
  $('.crsl-items').carousel({
    visible: 2,
    itemMinWidth: 180,
    itemEqualHeight: 370,
    itemMargin: 30,
  });
  
  $("a[href=#]").on('click', function(e) {
    e.preventDefault();
  });
});

</script>
	<script type="text/javascript">
jQuery(function(f){
    var element = f('#fixedbanner');
    f(window).scroll(function(){
        element['fade'+ (f(this).scrollTop() > 300 ? 'In': 'Out')](500);           
    });
});
</script>	
<script>
  $(function () {
    $('#myTab a:last').tab('show')
  })
</script>		
		<script type="text/javascript" src="https://use.fontawesome.com/24abb777aa.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBXevj1w1GsPM7_9CllKJSWQQ0G6m5iO58"></script>
    <script type="text/javascript">
var map;
var egglabs = new google.maps.LatLng(53.82889, 34.410248);
var mapCoordinates = new google.maps.LatLng(53.82889, 34.410248);


var markers = [];
var image = new google.maps.MarkerImage(
    '/local/templates/krontif/images/map-marker.png',
    new google.maps.Size(70,80),
    new google.maps.Point(0,0),
    new google.maps.Point(42,56)
  );

function addMarker() 
{
      markers.push(new google.maps.Marker({
      position: egglabs,
      raiseOnDrag: false,
	  icon: image,
      map: map,
      draggable: false
      }));
      
}




function initialize() {
  var mapOptions = {
	backgroundColor: "#e0e0e0",
    zoom: 14,
	disableDefaultUI: true,
    center: mapCoordinates,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
	styles: [
			  {
			    "featureType": "landscape.natural",
			    "elementType": "geometry.fill",
			    "stylers": [
			      { "color": "#e0e0e0" }
			    ]
			  },
			  {
				    "featureType": "landscape.man_made",
				    "stylers": [
				      { "color": "#000000" },
				      { "visibility": "off" }
				    ]
			  },
			  {
				    "featureType": "water",
				    "stylers": [
				       { "color": "#d4d4d4" },
				      { "saturation": 0 }
				    ]
			  },
			  {
				    "featureType": "road.arterial",
				    "elementType": "geometry",
				    "stylers": [
				      { "color": "#ffffff" }
				    ]
			  }
			 ,{
				    "elementType": "labels.text.stroke",
				    "stylers": [
				      { "visibility": "off" }
				    ]
			  }
				,{
				    "elementType": "labels.text",
				    "stylers": [
				      { "color": "#000000" }
				    ]
				  }
				
				,{
				    "featureType": "road.local",
				    "stylers": [
				      { "color": "#ffffff" }
				    ]
				  }
				,{
				    "featureType": "road.local",
				    "elementType": "labels.text",
				    "stylers": [
				      { "color": "#000000" }
				    ]
				  }
				,{
				    "featureType": "transit.station.bus",
				    "stylers": [
				      { "saturation": -57 }
				    ]
				  }
				,{
				    "featureType": "road.highway",
				    "elementType": "labels.icon",
				    "stylers": [
				      { "visibility": "off" }
				    ]
				  },{
				    "featureType": "poi",
				    "stylers": [
				      { "visibility": "off" }
				    ]
				  }
			
			]
    
  };
map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
addMarker();
 
}
google.maps.event.addDomListener(window, 'load', initialize);

    </script>
	
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>

<script>
$(document).ready(function(){
	$("#mtel").mask("+7 ?(999) 999 99 99");
	$(".mtel").mask("+7 ?(999) 999 99 99");
})
</script>

<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter48977048 = new Ya.Metrika({ id:48977048, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true, ut:"noindex" }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/48977048?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123330077-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-123330077-1');
</script>

</body>
</html>