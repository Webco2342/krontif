<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
$theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);
$APPLICATION->SetTitle("PopUp");
   CJSCore::Init(array("popup"));
   CJSCore::Init(Array("viewer"));
?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
<?$APPLICATION->ShowHead()?>
<title><?$APPLICATION->ShowTitle()?></title>
	<?
	$APPLICATION->SetAdditionalCSS("/local/templates/krontif_new/css/bootstrap.min.css");
	$APPLICATION->SetAdditionalCSS("https://fonts.googleapis.com/css?family=Montserrat");
	$APPLICATION->SetAdditionalCSS("/local/templates/krontif_new/css/social-buttons.css");
	$APPLICATION->SetAdditionalCSS("/local/templates/krontif_new/css/jquery.jscrollpane.css");
	$APPLICATION->SetAdditionalCSS("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.css");	
	$APPLICATION->SetAdditionalCSS("/local/templates/krontif_new/css/slider.css");
	?>
   <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
   <script type="text/javascript" src="/local/templates/krontif/js/bootstrap.min.js"></script>
	
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
</head>

<body >


  <!-- Header --> 
  <div class="head" >
	<div class="head-menu" >	

	<nav class="navbar navbar-default navbar-inverse" role="navigation">
	<?$APPLICATION->ShowPanel();?>	
<div class="container">
<div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     
    </div>

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"catalog_horizontal_krontif", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_THEME" => "site",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "catalog_horizontal_krontif"
	),
	false
);?>

</div>
</div>
	</nav>

	</div>  
	<div class="head-logo" >
		<div class="container">
			<div class="row" >
				
				<div class="col-xs-12">
					<div class="phone_top">
						<img src="/local/templates/krontif_new/images/if_phone2_172518.png"> 
						<span class="title_phone">Телефон бесплатной горячей линии</span>
						<span class="number_phone">8 800 550 2820</span>
					</div>
				</div>
				<div class="col-md-6 col-xs-1">
					<a href="/"><img class="logo" src="/local/templates/krontif_new/images/logo.png"></a>
				</div>
				<div class="col-md-6 col-xs-11">
				<span class="slogan">Мы в ответе за тех, кто без газа!</span>
				</div>
			</div>
		</div>
	</div>

<? if ($APPLICATION->GetCurPage(false) === '/'): ?>
      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="8000">
        <!-- Контейнер обертка для слайдов Wrapper-->
        <div class="carousel-inner" role="listbox">
		<!--- <div class="item active">
			<svg width="100%" height="100%" class="pie_banner" version="1.2" viewBox="0 0 1920 580" preserveAspectRatio="xMidYMid meet">
			<image class="banner_1" xlink:href="/local/templates/krontif_new/images/bg_banner_4.png" width="1920" height="528" />
		  <svg width="100%" height="100%" viewBox="0 0 460 210" preserveAspectRatio="xMidYMid meet">
		  <text class="molot" x="50"  y="25" fill="#37373b" font-size="22px" font-weight="100">
			ВНИМАНИЕ!
			<tspan dx="-120" dy="20" class="comp" fill="#37373b" font-size="12px">
			ОБЪЯВЛЯЕТСЯ 
			</tspan>
			<tspan dx="-70" dy="20" class="molot" fill="#37373b" font-size="18px">
			KRONTIF 
			</tspan>
			<tspan dx="-7" dy="0" class="comp" fill="#37373b" font-size="12px">
			O 
			</tspan>
			<tspan dx="-2" dy="0" class="comp" fill="#ed1c24" font-size="12px">
			МАНИЯ
			</tspan>
			<tspan dx="0" dy="0" class="molot" fill="#37373b" font-size="40px">
			!
			</tspan>
		  </text>
		  
		  <text class="comp" x="200"  y="40" fill="#fff" font-size="20px" font-weight="300">
			Купи
			<tspan dx="0" dy="0" class="comp" fill="#fff" font-size="20px">
			котел серии 
			</tspan>
			<tspan dx="0" dy="0" class="molot" fill="#fff" font-size="25px">
			«СИБИРЬ»
			</tspan>
			<tspan dx="-230" dy="20" class="comp" fill="#fff" font-size="20px">
			мощностью от 21 до 50 кВт и получи 
			</tspan>
			<tspan dx="-2" dy="0" class="molot" fill="#fff" font-size="20px">
			ПОДАРОК
			</tspan>
			<tspan dx="-300" dy="30" class="comp" fill="#fff" font-size="20px">
			набор инструментов STELS (Германия)
			</tspan>
			<tspan dx="-270" dy="30" class="comp" fill="#fff" font-size="20px">
			c пожизненной гарантией.
			</tspan>
		  </text>

		  </svg>
		   <image x="1312"  y="260" xlink:href="/local/templates/krontif_new/images/banner_u_r_b.png" width="608" height="320" />
		   <image x="1590"  y="185" xlink:href="/local/templates/krontif_new/images/stels.png" width="320" height="320" />
		  <text class="comp" x="1050"  y="450" fill="#fff" font-size="25px" font-weight="100">
		  Подробнее с условиями акции можно ознакомиться <a class="a_svg" xlink:href="/consumers/salle/"> здесь</a>
		 </text>
		   
			</svg>		
		</div> --->
        <div class="item active">
		<svg width="100%" height="100%" class="pie_banner" version="1.2" viewBox="0 0 1920 580" preserveAspectRatio="xMidYMid meet">
          <image class="banner_1" xlink:href="/local/templates/krontif_new/images/bg_banner_1.png" width="1920" height="528" />		  
		  <rect x="0" y="0" fill="#ed1c24" width="100%" height="88">
		  </rect>
		  <svg width="100%" height="100%" viewBox="0 0 740 330" preserveAspectRatio="xMidYMid meet">
		  <text x="5"  y="40" fill="transparent" font-size="45px" font-weight="100">
			Знакомьтесь, КОТЛЫ
			<tspan dx="10" dy="10" class="molot" fill="#37373b" font-size="70px">
			KRONTIF!
			</tspan>
		  <animate attributeName="fill" 
		  from="transparent" 
		  to="#f3d2b2" begin="0s" dur="0.1s" fill="freeze">
		  </animate>
		  </text>
		  </svg>

		  <defs>
				<linearGradient id="line">
					    <stop offset="0%" stop-color="#ed1c24" 
							  class="stop"/>   
					
						<stop offset="100%" stop-color="#ad181e"
							  class="stop-2"/>
				</linearGradient>
		  </defs>
		
		<g>
		  <path fill="url(#line)" class="tr_el_1" d="M 450 220 L 750 120 L 1650 120 L 1650 145 L 750 145 L 450 220" > 
		  
		  </path>
		  <text x="800"  y="142" fill="transparent" class="molot" font-size="25px" font-weight="100">
		  высокое качество литья
		  <animate attributeName="fill" 
		  from="transparent" 
		  to="#f3d2b2" begin="1s" dur="0.1s" fill="freeze">
		  </animate>		  
		  </text>
		</g> 
		
		<g>
		  <path fill="url(#line)" class="tr_el_1" d="M 450 245 L 750 170 L 1550 170 L 1550 195 L 750 195 L 450 245">
		  </path>
		  <text x="880"  y="192" fill="transparent" class="molot" font-size="19px" font-weight="100">
		  чугун собственного производства
		  <animate attributeName="fill" 
		  from="transparent" 
		  to="#f3d2b2" begin="1.5s" dur="0.1s" fill="freeze">
		  </animate>		  
		  </text>
		</g>  
		
		<g>
		  <path fill="url(#line)" class="tr_el_1" d="M 450 270 L 750 220 L 1550 220 L 1550 245 L 750 245 L 450 270">
		  </path>
		  <text x="770"  y="242" fill="transparent" class="molot" font-size="21px" font-weight="100">
		  простой в эксплуатации
 		  <animate attributeName="fill" 
		  from="transparent" 
		  to="#f3d2b2" begin="2s" dur="0.1s" fill="freeze">
		  </animate>
		  </text>
		</g>  
		
		<g>
		  <path fill="url(#line)" class="tr_el_1" d="M 450 295 L 750 270 L 1550 270 L 1550 295 L 750 295 L 450 295"> 

		  </path>
		  <text x="750"  y="292" fill="transparent" class="molot" font-size="18px" font-weight="100">
		  надежный производитель с 280-летней историей
		  <animate attributeName="fill" 
		  from="transparent" 
		  to="#f3d2b2" begin="2.5s" dur="0.1s" fill="freeze">
		  </animate>		  
		  </text>		  
		</g>  
		
		<g>
		  <path fill="url(#line)" class="tr_el_1" d="M 450 320 L 750 320 L 1550 320 L 1550 345 L 750 345 L 450 320">
		  </path>
		  <text x="900"  y="342" fill="transparent" class="molot" font-size="26px" font-weight="100">
		  3,5 года гарантии
		  <animate attributeName="fill" 
		  from="transparent" 
		  to="#f3d2b2" begin="3s" dur="0.1s" fill="freeze">
		  </animate>		  
		  </text>		  
		</g>  
		
		<g>
		  <path fill="url(#line)" class="tr_el_1" d="M 450 345 L 750 370 L 1550 370 L 1550 395 L 750 395 L 450 345">

		  </path>
		  <text x="800"  y="392" fill="transparent" class="molot" font-size="18px" font-weight="100">
		  высокие технические характеристики
		  <animate attributeName="fill" 
		  from="transparent" 
		  to="#f3d2b2" begin="3.5s" dur="0.1s" fill="freeze">
		  </animate>		  
		  </text>
		</g>  
		
		<g>
		  <path fill="url(#line)" class="tr_el_1" d="M 450 370 L 750 420 L 1600 420 L 1600 445 L 750 445 L 450 370">
		  </path>
		  <text x="910"  y="442" fill="transparent" class="molot" font-size="25px" font-weight="100">
		  неприхотливы, всеядны
		  <animate attributeName="fill" 
		  from="transparent" 
		  to="#f3d2b2" begin="4s" dur="0.1s" fill="freeze">
		  </animate>		  
		  </text>		  
		</g>
		
		  <image x="1500" y="10" width="220px" xlink:href="/local/templates/krontif_new/images/banner_hi.png" width="219" height="493" >
		  </image>
		  <g >
		  <image x="70" y="95" id="kotel" width="349px" xlink:href="/local/templates/krontif_new/images/kotel_banner_1.png" width="349" height="391" >
		  </g>
		</svg>
        </div>
		
        <div class="item">
         <svg width="100%" height="100%" class="pie_banner" version="1.2" viewBox="0 0 1920 580" preserveAspectRatio="xMidYMid meet">
          <image xlink:href="/local/templates/krontif_new/images/bg_banner_2.png" width="1920" height="528" />
		   <text x="260"  y="125" fill="#f3d2b2" class="molot" font-size="40px" font-weight="100">
		  Товарищ, помни! <tspan class="molot" dx="10" font-size="65px" dy="0"> Скупой платит дважды!</tspan>
		  <tspan class="molot" dx="-1200" dy="80" fill="#000" font-size="65px"> «Демидовъ»,«Сибирь»
		  
		  </tspan> <tspan class="molot" dx="10" dy="0">- для сознательных Граждан!</tspan>
		  <tspan class="molot" dx="-1380" dy="80" font-size="40px">
		  Высокое качество, доступный тариф!
		  </tspan>
		  <tspan class="molot" dx="-800" dy="80" font-size="62px">
		  Экономь и грейся с котлами
		  </tspan>		  
		  <tspan class="molot" dx="10" dy="0" fill="#000" font-size="110px">KRONTIF!</tspan>
		  <animate attributeName="fill" 
		   begin="5;.click" values="#f3d2b2;#ffffff;#f3d2b2" dur="15s" fill="freeze" repeatCount="indefinite">
		  </animate>
		  
		  </text>	
		 </svg>
        </div>
        <div class="item">
         <svg width="100%" height="100%" class="pie_banner" version="1.2" viewBox="0 0 1920 580" preserveAspectRatio="xMidYMid meet">
          <image xlink:href="/local/templates/krontif_new/images/bg_banner_3.png" width="1920" height="528" />
		  <image x="584" y="2" xlink:href="/local/templates/krontif_new/images/banner_3_kotel.png" width="439" height="450" >
		  <animate attributeName="x" 
		  from="584" 
		  to="594" begin="1.3s" dur="2s" fill="freeze" repeatCount="indefinite" >
		  </animate> 
		  </image>
		 
		  <text x="1062"  y="224" fill="#fff" class="molot" font-size="95px">
		  Зарегистрируй 
		   <tspan class="myriad" dx="-30" fill="#2a0080" dy="-40" font-size="48px" font-weight="900"><a class="a_svg" xlink:href="/consumers/registration/">тут</a></tspan> 
		  <tspan class="comp" dx="-200" fill="#000" dy="110" font-size="81px" font-weight="900">
		  ПОЛУЧИ
		  </tspan>
		  <tspan class="comp" dx="-420" fill="#000" dy="40" font-size="41px" font-weight="900">
		  ГОД
		  </tspan>
		  <tspan class="comp" dx="10" fill="#000" dy="0" font-size="41px" font-weight="100">
		  дополнительной гарантии
		  </tspan>
		  </text>
		 
		  <image x="1770" y="60" xlink:href="/local/templates/krontif_new/images/banner_3_ruka.png" width="74" height="85" >
		  <animate attributeName="y" 
		  from="60" 
		  to="70" begin="1s" dur="0.5s" fill="freeze" repeatCount="indefinite" >
		  </animate> 
		  </image>
		 </svg>
        </div>        
        </div>
        <!-- Стрелки вперед назад расположенные слева и справа карусели -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Следующий</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Предыдущий</span>
        </a>
      </div>
<?elseif($APPLICATION->GetCurPage(false) !== '/'):?>	  
	 <div class="str"> 
	 
	 </div>
<?endif?>
         <div class="carousel-menu col-xs-12" >
			<div class="registration col-xs-12 col-md-3"><a href="/consumers/registration/"><img src="/local/templates/krontif_new/images/icon_4.png"> <h2>Регистрация <br> котла</h2></a></div>
			<div class="salle col-xs-12 col-md-3"><a href="/consumers/salle/"><img src="/local/templates/krontif_new/images/icon_2.png"> <h2>Акции</h2></a></div>
			<div class="maps col-xs-12 col-md-3"><a href="/consumers/service/guarantee/"><img src="/local/templates/krontif_new/images/icon_1.png"> <h2>Гарантийные<br> условия</h2></a></div>
			<div class="advantages col-xs-12 col-md-3"><a href="/consumers/calculator/"><img src="/local/templates/krontif_new/images/icon_3.png"> <h2>Калькулятор<br> мощности</h2></a></div>			
        </div> 
<?if($APPLICATION->GetCurPage(false) !== '/'):?>		
	<div class="container">
		<div class="row">		
			<div class="main-breadcrumb col-xs-12" >
				<?$APPLICATION->IncludeComponent(
					"bitrix:breadcrumb",
					"",
					Array(
						"PATH" => "",
						"SITE_ID" => "s1",
						"START_FROM" => "0"
					)
				);?>
				<h1 class="title-content"><?$APPLICATION->ShowTitle(false)?></h1>
						
<?if($APPLICATION->GetCurPage(false) === '/about/history/'):?>
<center><img class="hictory_tytle_img" src="/upload/history-top.png"></center>
<?endif?>
			</div>
		</div>
	</div>
  </div>
  
<div class="<?if($APPLICATION->GetCurPage(false) !== '/consumers/registration/' && $APPLICATION->GetCurPage(false) !== '/consumers/catalog/series-siberia/gabaritnye-razmery-kotlov.php'):?>bg_content<?else:?> bg_catalog_index_hit_clic<?endif?>">
<?if($APPLICATION->GetCurPage(false) !== '/about/history/'):?>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
	
<?endif?>	
<?endif?>	