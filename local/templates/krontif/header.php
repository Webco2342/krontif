<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
$theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);
?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
<?$APPLICATION->ShowHead()?>
<title><?$APPLICATION->ShowTitle()?></title>
	<?
	$APPLICATION->SetAdditionalCSS("/local/templates/krontif/css/bootstrap.min.css");
	$APPLICATION->SetAdditionalCSS("https://fonts.googleapis.com/css?family=Montserrat");
	$APPLICATION->SetAdditionalCSS("/local/templates/krontif/css/social-buttons.css");
	$APPLICATION->SetAdditionalCSS("/local/templates/krontif/css/jquery.jscrollpane.css");
	$APPLICATION->SetAdditionalCSS("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.css");	
	?>
</head>

<body >


  <!-- Header --> 
  <div class="head" >
	<div class="head-menu" >	

	<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
	<?$APPLICATION->ShowPanel();?>	
<div class="container">
<div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     
    </div>

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"horizontal", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_THEME" => "site",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "horizontal"
	),
	false
);?>

</div>
</div>
	</nav>

	</div>  
	<div class="head-logo" >
		<div class="container">
			<div class="row" >
				<div class="col-xs-12"><img class="logo" src="/local/templates/krontif/img/logo-krontif.png"></div>
			
			</div>
		</div>
	</div>


  
<div class="bg-78 war">
		<div class="container">
			<div class="row">
				

				

				<div class="col-md-12 block">
				<h3 class="title-content">Информация на странице ограничена. За дополнительной информацией обращайтесь к нашим сотрудникам. Основной сайт на реконструкции.</h3>	
				
				
				</div>				
				
			</div>
		</div>	
	</div>
  <div class="wrapper wrapper--demo">					
	<?/* div class="carousel">
	
		<div class="carousel__content">
		
		<div class="item">
        <p class="title">
		</p>
        <img src="https://lemax-kotel.ru/images/slider/register.jpg" alt="">
		</div>
		
		<div class="item">
        <p class="title"></p>
        <img src="https://lemax-kotel.ru/images/slider/new_kotel.jpg" alt="">
		</div>
		<div class="item">
        <p class="title"></p>
        <img src="https://lemax-kotel.ru/images/slider/closer.jpg" alt="">
		</div>

		</div>
    
    <div class="carousel__nav">
      <a href="#" class="nav nav--left "><div class="glyphicon glyphicon-chevron-left"></div></a>
      <a href="#" class="nav nav--right"><div class="glyphicon glyphicon-chevron-right"></div></a>
    </div>
    
	</div*/?>
</div>
	

  </div>
