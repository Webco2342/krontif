<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="bg-pers">	
<div class="block-pers bg-78">	
	<div class="container">
		<div class="row">	
		<h3 id="contact" class="title-content">Для получения дополнительной информации свяжитесь с нами:
</h3>
		<div class="col-md-1">
		</div>
			<div class="col-md-10">
	<center>		<h2>Ирина Прохорова</h2>

<p>E-mail: prohorova@metholding.com</p>
<p>Web-site: metholding.com</p>
<p>Web-site: kotel-krontif.ru, krontif.ru</p>
<p>Тел.: +7 495 725 56 82 (доб. 321)</p>
<p>Моб.: +7 915 270 53 71</p>
</center>
			</div>


		
		</div>
	</div>
</div>	
<div class="bg-78">	
<div class="contacts">
	<div class="container">
		<div class="row">	
		<h3 class="title-content">Контакты производства:</h3>
			 	
		</div>	
<div class="block blok contacts-tab">	
	<ul class="nav nav-tabs nav-justified" id="myTab" >
	  <li class="active"><a href="#adres" data-toggle="tab" class="btn btn-social-icon btn-lg"><span class="fa fa-map-marker"></span></a></li>
	  <li><a href="#phone" data-toggle="tab" class="btn btn-social-icon btn-lg"><span class="fa fa-phone"></span></a></li>
	  <li><a href="#time" data-toggle="tab" class="btn btn-social-icon btn-lg"><span class="fa fa-clock-o"></span></a></li>
	</ul>
	
<div class="tab-content nav-justified">
  <div class="tab-pane active" id="adres">
  <center>
  <strong>Адрес</strong>
<p>249401, Россия, Калужская область,<br>  г. Людиново, ул. Щербакова, 1а</p>
</center>
  </div>
  <div class="tab-pane" id="phone">
  <center>
  <strong>Контактный телефон</strong>
  <p>+7 (48444) 6-92-87</p>
 <strong>E-mail</strong>
 <p>sbytKC@krontif.ru</p>
<center>
  </div>
  <div class="tab-pane" id="time">
  <center>
  <strong>Время работы</strong>
  <p>пн - пт с 9:00 до 18:00</p>
  <center>
  </div>
</div>
</div>	
	</div>
</div>	
<div id="map-canvas"></div>
<!--
			<div class="col-md-12">
			<h3 class="title-content">Мы в социальных сетях</h3>
			</div>
			<div class="col-md-4">
			</div>
			<div class="col-md-4 soc">
			<center>
			<a class="btn btn-social-icon btn-lg btn-facebook">
				<span class="fa fa-facebook"></span>
			</a>
			<a class="btn btn-social-icon btn-lg btn-odnoklassniki">
				<span class="fa fa-odnoklassniki"></span>
			</a>
			<a class="btn btn-social-icon btn-lg btn-vk">
				<span class="fa fa-vk"></span>
			</a>		
			<a class="btn btn-social-icon btn-lg btn-instagram">
				<span class="fa fa-instagram"></span>
			</a>
			</center>			
			</div>	
-->
</div>

</div>

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="/local/templates/krontif/js/bootstrap.min.js"></script>
	
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>


<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.transit/0.9.9/jquery.transit.min.js'></script>

		<script type="text/javascript" src="/local/templates/krontif/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="/local/templates/krontif/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="/local/templates/krontif/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>	
	    <script type="text/javascript" src="/local/templates/krontif/js/slider.js"></script>	
<script>
  $(function () {
    $('#myTab a:last').tab('show')
  })
</script>		
		<script type="text/javascript" src="https://use.fontawesome.com/24abb777aa.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBXevj1w1GsPM7_9CllKJSWQQ0G6m5iO58"></script>
    <script type="text/javascript">
var map;
var egglabs = new google.maps.LatLng(53.82889, 34.410248);
var mapCoordinates = new google.maps.LatLng(53.82889, 34.410248);


var markers = [];
var image = new google.maps.MarkerImage(
    '/local/templates/krontif/images/map-marker.png',
    new google.maps.Size(70,80),
    new google.maps.Point(0,0),
    new google.maps.Point(42,56)
  );

function addMarker() 
{
      markers.push(new google.maps.Marker({
      position: egglabs,
      raiseOnDrag: false,
	  icon: image,
      map: map,
      draggable: false
      }));
      
}




function initialize() {
  var mapOptions = {
	backgroundColor: "#ffffff",
    zoom: 14,
	disableDefaultUI: true,
    center: mapCoordinates,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
	styles: [
			  {
			    "featureType": "landscape.natural",
			    "elementType": "geometry.fill",
			    "stylers": [
			      { "color": "#ffffff" }
			    ]
			  },
			  {
				    "featureType": "landscape.man_made",
				    "stylers": [
				      { "color": "#000000" },
				      { "visibility": "off" }
				    ]
			  },
			  {
				    "featureType": "water",
				    "stylers": [
				       { "color": "#337ab7" },
				      { "saturation": 0 }
				    ]
			  },
			  {
				    "featureType": "road.arterial",
				    "elementType": "geometry",
				    "stylers": [
				      { "color": "#c5c7c6" }
				    ]
			  }
			 ,{
				    "elementType": "labels.text.stroke",
				    "stylers": [
				      { "visibility": "off" }
				    ]
			  }
				,{
				    "elementType": "labels.text",
				    "stylers": [
				      { "color": "#000000" }
				    ]
				  }
				
				,{
				    "featureType": "road.local",
				    "stylers": [
				      { "color": "#c5c7c6" }
				    ]
				  }
				,{
				    "featureType": "road.local",
				    "elementType": "labels.text",
				    "stylers": [
				      { "color": "#000000" }
				    ]
				  }
				,{
				    "featureType": "transit.station.bus",
				    "stylers": [
				      { "saturation": -57 }
				    ]
				  }
				,{
				    "featureType": "road.highway",
				    "elementType": "labels.icon",
				    "stylers": [
				      { "visibility": "off" }
				    ]
				  },{
				    "featureType": "poi",
				    "stylers": [
				      { "visibility": "off" }
				    ]
				  }
			
			]
    
  };
map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
addMarker();
 
}
google.maps.event.addDomListener(window, 'load', initialize);

    </script>	
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>

<script>
$(document).ready(function(){
	$("#mtel").mask("+7 ?(999) 999 99 99");
	
})
</script>
</body>
</html>