<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Завод сегодня");
CJSCore::Init(Array("viewer"));
?>
<div class="video_about">
<?$APPLICATION->IncludeComponent(
	"bitrix:player",
	"",
	Array(
		"ADVANCED_MODE_SETTINGS" => "N",
		"AUTOSTART" => "N",
		"HEIGHT" => "495",
		"MUTE" => "N",
		"PATH" => "/about/zavod-segodnya/PMH_KRONTIF.mp4",
		"PLAYBACK_RATE" => "1",
		"PLAYER_ID" => "",
		"PLAYER_TYPE" => "auto",
		"PRELOAD" => "Y",
		"PREVIEW" => "/about/zavod-segodnya/vidio_jpg.jpg",
		"REPEAT" => "none",
		"SHOW_CONTROLS" => "Y",
		"SIZE_TYPE" => "absolute",
		"SKIN" => "",
		"SKIN_PATH" => "/bitrix/components/bitrix/player/videojs/skins",
		"START_TIME" => "0",
		"USE_PLAYLIST" => "N",
		"VOLUME" => "50",
		"WIDTH" => "1145"
	)
);?>
</div>
<p style="margin-top:50px">АО «Кронтиф-Центр» («Сукремльский чугунолитейный завод») - одно из старейших литейных предприятий страны с 280-летним опытом и традициями славных русских мастеров. Сегодня предприятие обладает собственной сырьевой базой, современными производственными мощностями и репутацией надежного Производителя не только на отечественном рынке, но и в Европе.</p>

<p>Наше предприятие производит промышленные и потребительские изделия из чугунного литья:

      <ul class="krontif_ul"> 
	  
	   <li>Мелющие шары для горно-обогатительных комбинатов;</li>

       <li>Канализационные люки и дождеприемники различных видов;</li>

       <li>Художественное литье;</li>

       <li>Раструбные и безраструбные трубы,</li>

       <li>Отопительные водогрейные котлы с чугунным теплообменником.</li>
	   
	  </ul>
</p>
 Устойчивый рост и крепкие позиции на рынке чугунного литья позволяют АО «Кронтиф-центр» уверенно смотреть в будущее и создавать         новые товарные направления. В 2015 году мы начали работу над созданием котлов чугунных малометражных (КЧМ), которые бы учитывали особенности национального рынка. В результате мы разработали две серии чугунных котлов под торговой маркой KRONTIF – «ДемидовЪ» и «Сибирь», акцент в которых мы сделали на качестве, эффективности и приемлемой цене.
</div>
</div>
</div>
</div>
<div class="bg_blue" id="db-items">
	<div class="col-md-2">
	
	</div>
	<div class="col-md-8 ">
	

	<div class="col-md-4">
	<center>
		<img width="95%"
		 onload="this.parentNode.className='feed-com-img-wrap';"
            src="СМК_2017-3-250.jpg"
            data-bx-viewer="image" 
            data-bx-title="" 
            data-bx-src="СМК_2017-3.jpg" 
            data-bx-width="1024" 
            data-bx-height="1450" 
		>
	</center>
	</div>	
	<div class="col-md-4">
	<center>
		<img width="95%"
		 onload="this.parentNode.className='feed-com-img-wrap';"
            src="СМК_2017-4-250.jpg"
            data-bx-viewer="image" 
            data-bx-title="" 
            data-bx-src="СМК_2017-4.jpg" 
            data-bx-width="1024" 
            data-bx-height="1450" 
		>
	</center>
	</div>
	<div class="col-md-4">	
	<center>
		<img width="95%"
		 onload="this.parentNode.className='feed-com-img-wrap';"
            src="СМК_2017-5-250.jpg"
            data-bx-viewer="image" 
            data-bx-title="" 
            data-bx-src="СМК_2017-5.jpg" 
            data-bx-width="1024" 
            data-bx-height="1450" 
		>
	</center>	
	</div>
	
</div>	
</div>
<div >
	<div >
		<div >
			<div >
<script>
BX.ready(function(){
   var obImageView = BX.viewElementBind(
      'db-items',
      {showTitle: true, lockScroll: false},
      function(node){
         return BX.type.isElementNode(node) && (node.getAttribute('data-bx-viewer') || node.getAttribute('data-bx-image'));
      }
   );
});
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>