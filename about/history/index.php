<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("История");
?>
<div class="st">
<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 500 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="500" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
19 июля 1738
</text>
</svg>
</svg>
</span>
<center><div style="margin:40px">Основание завода крупнейшим <br>
промышленником той поры - <b>НИКИТОЙ ДЕМИДОВЫМ</b></div></center>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
1820 
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
 Демидовы передали предприятие другой известной промышленной династии – Мальцовым. С середины 20-х годов завод приступил к производству канализационных труб и фасонных деталей.
 

			</div>
		</div>
	</div>
</div>



<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 400 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="400" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
1941-1945
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Запущена первая вагранка; освоен центробежный способ отливки канализационных труб; введены в эксплуатацию сначала однороторные машины, а затем – многороторные автоматические машины; внедрена кокильная заливка фасонных частей.
 <center><img class="hictory_tytle_img" src="/local/templates/krontif_new/images/history-1.png"></center>
			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 400 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="400" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
1970-Е
</text>
</svg>
</svg>
</span> 

<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			Сукремльский чугунолитейный завод становится лидером отечественной промышленности по выпуску сантехнической продукции — ее получали более 300 городов Советского Союза, а также 19 стран по всему миру.
			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 400 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="400" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
1980-Е
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
  Построены и пущены в эксплуатацию два цеха, автоматизированная линия по отливке кронштейнов, сифонов и фасонных изделий, крупнейший в Европе цех по производству чугунных труб.
			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 400 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="400" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
1990-Е
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
 Завод переименовался в Акционерное общество «Сукремльский чугунолитейный завод». В этот период были разработаны новые виды изделий – мелющие шары для горно-обогатительных комбинатов и отливки из высокопрочного чугуна. 
 			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
1997 
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
 На предприятии началось производство ливневой канализации для английской фирмы «Hargreaves foundry LTD», тесное сотрудничество с которой продолжается до сих пор.
 <center><img class="hictory_tytle_img" src="/local/templates/krontif_new/images/history-2.png"></center>
			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
2003
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
 Завод вошел в состав компании крупнейшего металлургического холдинга России -  «Промышленно-металлургический холдинг» и получил название АО «Кронтиф-Центр». </p>
			</div>
		</div>
	</div>
</div>
<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
2004
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			Завод впервые за последние двенадцать лет перешагнул планку в 50 тысяч тонн по выпуску чугунного литья в год, а в течение следующих 3-х лет достиг объемов производства в 77 тыс. тонн.  
			</div>
		</div>
	</div>
</div>
<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
2005
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			На предприятии проведен сертификационный аудит системы менеджмента качества (СМК) на предмет соответствия требованиям международного стандарта ISO 9001:2000 и получен Сертификат соответствия.
			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
2007
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			Выпуск товарной продукции впервые перешагнул миллиардный рубеж.
			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
2013
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			Введена в эксплуатацию комплексная автоматическая формовочная линия Omega Foundry Machinery LTD (Великобритания). Данная формовочная линия предназначена для изготовления отливок по технологии ХТС. 
			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
2014
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			 Введена в эксплуатацию автоматическая формовочная линия HSP-3D (Германия) с размером опок 1000х800 мм и производительностью 50 форм/ч. 
			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
2015
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<p>
			Введена в эксплуатацию автоматическая линия на базе формовочной машины FDNX (Япония) c размером производимых форм 500х400 мм и производительностью 80 форм/ч.
			</p>
			<p>
			Началась работа по созданию новой товарной линейки – отопительных твердотопливных чугунных котлов под торговой маркой KRONTIF. Линейка представлена в двух сериях «ДемидовЪ» и «СИБИРЬ».
			</p>
			 <center><img class="hictory_tytle_img" src="/local/templates/krontif_new/images/history-3.png"></center>
			</div>
		</div>
	</div>
</div>

<span class="history_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
2017
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			Запуск серийного производства котлов KRONTIF. Выход на отечественный рынок.
			</div>
		</div>
	</div>
</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>