<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
if(CModule::IncludeModule("iblock"))
?>
<?
$i=0;
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_PICTURE", "DETAIL_TEXT", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>"5", "ID"=>$_GET["id"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("DATE_ACTIVE_FROM"=>"DESC"), $arFilter, false, Array("nPageSize"=>4), $arSelect);
$arResult["NAV_STRING"] = $res->GetPageNavStringEx($navComponentObject, "", $arParams["PAGER_TEMPLATE"]);
while($ob = $res->GetNextElement()){ 

 $arFields = $ob->GetFields();
$arDATE = ParseDateTime($arFields["DATE_ACTIVE_FROM"], FORMAT_DATETIME); 

?>

<div class="news_detail">
	<div class="col-md-12 date">
	<h2><?=$arFields["NAME"]?></h2>
	<span><?=$arDATE["DD"]?>.
	<?=$arDATE["MM"]?>.<?=$arDATE["YYYY"]?></span>
	</div>
	
	<div class="col-md-12">
	<img src="<?= CFile::GetPath($arFields["DETAIL_PICTURE"])?>">
		<?=$arFields["DETAIL_TEXT"]?>
	</div>
</div>


<?
$i++;
};

?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>