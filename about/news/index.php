<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
if(CModule::IncludeModule("iblock"))
?>
<?
$i=0;
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_PICTURE", "PREVIEW_TEXT", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>"5", "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("DATE_ACTIVE_FROM"=>"DESC"), $arFilter, false, Array("nPageSize"=>4), $arSelect);
$arResult["NAV_STRING"] = $res->GetPageNavStringEx($navComponentObject, "", $arParams["PAGER_TEMPLATE"]);
while($ob = $res->GetNextElement()){ 

 $arFields = $ob->GetFields();
$arDATE = ParseDateTime($arFields["DATE_ACTIVE_FROM"], FORMAT_DATETIME); 

?>

<div class="col-md-12 <?if($i==0){?>news_top<?}else{?>news_list<?}?>">
	<div class="col-md-1 date">
	<span><?=$arDATE["DD"]?></span>
	<?=$arDATE["MM"]?>.<?=$arDATE["YYYY"]?>
	</div>
	<div class="col-md-11 prev">
	<img src="<?= CFile::GetPath($arFields["PREVIEW_PICTURE"])?>">
	<a href="/about/news/detail.php?id=<?=$arFields["ID"]?>"><?=$arFields["NAME"]?></a>
	<?=$arFields["PREVIEW_TEXT"]?>
	</div>
</div>
<div class="col-md-12 news_clic">
<div class="col-md-1">

</div>
<div class="col-md-11">
<a href="/about/news/detail.php?id=<?=$arFields["ID"]?>" class="btn btn-danger" >Читать далее</a>
</div>
</div>

<?
$i++;
};
/*echo $arResult["NAV_STRING"];*/
?>
<?if($i==false){?>
<h3>В данном разделе скоро появятся новости завода KRONTIF.</h3>
<?}else{?>
<?if($_GET[SHOWALL_1]!=1){?>
<div class="right">
<a href="/about/news/?SHOWALL_1=1">
						<svg class="pie_form" version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">		
						<defs>
							<linearGradient id="gradient">
								<stop offset="0%" style="stop-color: #ed1c24"></stop>
								<stop offset="100%" style="stop-color: #97080e"></stop>
							</linearGradient>
						</defs>
					<path class="tr_el_1" d="M 0 45 L 335 0 L 100 155  z"> </path>
					<path class="tr_el_2" fill="url(#gradient)" d="M 60 25 L 250 25 L 250 107 L 32 107  z"> </path>
					<text x="71" y="75" fill="#fff" font-size="30">Все новости </text>
					</svg>	
						
						</a>
</div>
<?}}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>