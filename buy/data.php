<?
function file_get_contents_from_url($url){

    $data = file_get_contents($url);

    if ($data === false){

        if (function_exists('curl_init')){

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            $data = curl_exec($curl);
            curl_close($curl);

        }

    }

    return $data;

}

if(CModule::IncludeModule("iblock"))
$json = file_get_contents("data.json");

$arSelect = Array("ID", "IBLOCK_ID", "TIMESTAMP_X");
$arFilter = Array("IBLOCK_ID"=>6, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");// 8 - заменяем на ID инфоблока
$res = CIBlockElement::GetList(Array("TIMESTAMP_X"=>"DESC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
$ar_fields = $res->GetNext();
$date_1 = $ar_fields['TIMESTAMP_X'];


$filename = 'data.json';
if (file_exists($filename)) {
$date_2 = date("d.m.Y H:i:s.", filectime($filename));
}
if($date_1 >= $date_2){
$balun = array();
$i = 0;
$balun[type] = "FeatureCollection";
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>6, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1000), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
if(!$arProps[MAPS][VALUE]){
$map = json_decode(file_get_contents_from_url('https://geocode-maps.yandex.ru/1.x/?geocode='.$arProps[ADRES][VALUE].'&format=json&results=1'));
$center = $map->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos; 
$resmap = explode(" ", $center); 
$maps[0] = $resmap[1];
$maps[1] = $resmap[0];
}else{
$maps = explode(",", $arProps[MAPS][VALUE]);	
}

 foreach($arProps[TYPE][VALUE] as $type){
	 if($type == "Территориальный дистрибьютор"){
		$typ[0] = $type; 
	 }elseif($type == "Розничный магазин"){
		 $typ[1] = $type; 
	 }elseif($type == "Оптовые продажи"){
		 $typ[2] = $type; 
	 }elseif($type == "Интернет-магазин"){
		 $typ[3] = $type; 
	 }elseif($type == "Розничная сеть"){
		 $typ[4] = $type; 
	 }
 }

 $balun['features'][$i] = array(
				 "type" => "Feature",
				 "id" => IntVal($arFields["ID"]),
				 "geometry" => array(
									 "type" => "Point",
									 "coordinates" => array("0" => floatval($maps[0]), "1" => floatval($maps[1])),
								),
				 "properties" => array(
									 
									 "type" => $typ[0],
									 "type2" => $typ[1],
									 "type3" => $typ[2],
									 "type4" => $typ[3],
									 "type5" => $typ[4],
									 "balloonContentHeader" => $arFields["NAME"]." - ".$arProps[MAG][VALUE],
									 "balloonContentBody" => "<p>".$arProps[ADRES][VALUE]."<br>".$arProps[PHONE][VALUE]."</p>",
									 "balloonContentFooter" => "<a href='detail.php?id=".$arFields[ID]."'>Подробнее</a>",
									 "clusterCaption" => $arProps[MAG][VALUE],
									 "hintContent" => $arFields["NAME"],
								),
				 
			);
 /* {"type": "Feature", "id": 0, "geometry": {"type": "Point", "coordinates": [55.831903, 37.411961]}, "properties": {"balloonContent": "Аптека", "clusterCaption": "Аптека", "hintContent": "Аптека", "iconCaption": "Аптека"}, "options": {"preset": "islands#blueCircleDotIconWithCaption"}},*/
$i++;
 }

$file = file_get_contents('data.json');  // Открыть файл data.json
          
$taskList = json_decode($file,TRUE);        // Декодировать в массив 
                        
unset($file);                               // Очистить переменную $file
           
$taskList = $balun;        // Представить новую переменную как элемент массива, в формате 'ключ'=>'имя переменной'
          
file_put_contents('data.json',json_encode($taskList));  // Перекодировать в формат и записать в файл.
          
unset($taskList);

}

?>