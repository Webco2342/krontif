<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Где купить");
include("data.php");
if(CModule::IncludeModule("iblock"))
?>
  
<ul class="nav nav-tabs" id="myTab">
 
  <li ><a href="#list_region" data-toggle="tab">Показать списком</a></li>
 <li class="active"><a href="#list_map" data-toggle="tab">Искать на карте</a></li>
</ul>
<div class="tab-content">

<div class="tab-pane" id="list_region">
<?
$dillers = array();
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>6, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1000), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
 
 $dillers[$arProps[OKR][VALUE]][$arProps[REG][VALUE]] = $arFields[NAME].'-'.$arProps[CITY][VALUE];
 
}

foreach($dillers as $okr=>$reg){
	echo '<div class="col-sm-4 col-xs-12"><h3>'.$okr.'</h3><ul class="krontif_ul_3">';
	foreach($reg as $rev_res=>$city){
			echo '<li><a href="/buy/detail-list.php?REG='.$rev_res.'">'.$rev_res.'</a></li>';
	}
	echo '</ul></div>';
}
?>

</div>


<div class="tab-pane active" id="list_map">
<form class="form-horizontal" role="form" enctype="multipart/form-data"  method="POST" id="search" action="map-search.php" onsubmit="call()">	
<div class="row form-group search_maps_block">		
		
					<div class="col-xs-12">Найти ближайшего дилера “Кронтиф”</div> <input type="text" name="cit" class="col-xs-12" id="cit" placeholder="Город" > 
					<div class="form-group">					
					<p>Тип дилера:</p>	
						<div class="col-sm-4 col-xs-12 form-group-st">
						<label> 
							<input type="checkbox" class="checkbox" name="type" id="type" value="Территориальный дистрибьютор" > 
							<span class="checkbox-custom"></span>
							<span class="label">Территориальный дистрибьютор</span> 
						</label>
						</div>
						<div class="col-sm-4 col-xs-12 form-group-st">
						<label>
							<input type="checkbox" class="checkbox" name="typew" id="typew" value="Оптовые продажи" > 
							<span class="checkbox-custom"></span>
							<span class="label">Оптовые продажи</span> 
						</label>
						</div>
						<div class="col-sm-4 col-xs-12 form-group-st">
						<label>
							<input type="checkbox" class="checkbox" name="typer" id="typer" value="Розничная сеть" > 
							<span class="checkbox-custom"></span>
							<span class="label">Розничная сеть</span> 
						</label>
						</div>
						<div class="col-sm-4 col-xs-12 form-group-st">
						<label>
							<input type="checkbox" class="checkbox" name="typeq" id="typeq" value="Розничный магазин"> 
							<span class="checkbox-custom"></span>
							<span class="label">Розничный магазин</span> 
						</label>
						</div>
						<div class="col-sm-4 col-xs-12 form-group-st">
						<label>
							<input type="checkbox" class="checkbox" name="typee" id="typee" value="Интернет-магазин" > 
							<span class="checkbox-custom"></span>
							<span class="label">Интернет-магазин</span> 
						</label>
						</div>
					</div>	
				
					</div>					
					<div class="form-group right">
					<button type="submit"  id="searmap" class="btn btn-danger search_maps_btn">Искать</button>
					</div>
</form>	


<script type="text/javascript" language="javascript">
					
$(function(){
  $('#search').on('submit', function(e){
    e.preventDefault();
    var $that = $(this),
    formData = new FormData($that.get(0));				  
						$.ajax({						  
						  url: $that.attr('action'),
						  type: $that.attr('method'),
						  contentType: false, 
						  processData: false, 
						  data: formData,
						      
						  success: function(data) {
							$('#results-maps-search').html(data);
						  },
						 
						});
						});
						});
					
				</script>

<div id="results-maps-search">
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
 <script type="text/javascript">
ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map('map', {
            center: [50.76, 83.64],
            zoom: 4
        }, {
            searchControlProvider: 'yandex#search'
        }),
        objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32,
            clusterDisableClickZoom: true
        });
		myMap.controls.remove('searchControl');

    // Чтобы задать опции одиночным объектам и кластерам,
    // обратимся к дочерним коллекциям ObjectManager.
    objectManager.objects.options.set('preset', 'islands#greenDotIcon');
    objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
    myMap.geoObjects.add(objectManager);

	
	
	
    $.ajax({
        url: "data.json"
    }).done(function(data) {
        objectManager.add(data);
    });
	
	

}
window.onload = function() {
			var availableCity = [

									<?
									$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
									$arFilter = Array("IBLOCK_CODE"=>'CITY', "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
									$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5000), $arSelect);
									while($ob = $res->GetNextElement())
									{
									 $arFields = $ob->GetFields();
									 echo "'".$arFields['NAME']."', ";
									}
									?> 
										];
									jQuery( "#cit" ).autocomplete({source: availableCity});
								}
    </script> 
	
	 <div id="map" style="width: 100%; height: 942px"></div>
</div>					
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>