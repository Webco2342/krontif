<?
function file_get_contents_from_url($url){

    $data = file_get_contents($url);

    if ($data === false){

        if (function_exists('curl_init')){

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            $data = curl_exec($curl);
            curl_close($curl);

        }

    }

    return $data;

}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if(CModule::IncludeModule("iblock"))
$dillers = array();
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*", "DETAIL_PICTURE");
$arFilter = Array("IBLOCK_ID"=>6, "ID"=>$_GET['id'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
 $APPLICATION->SetTitle($arFields['NAME']);
 $APPLICATION->AddChainItem($arProps[REG][VALUE], "/buy/detail-list.php?REG=".$arProps[REG][VALUE]);
 $APPLICATION->AddChainItem($arFields['NAME'], " ");
 if(!$arProps[MAPS][VALUE]){
$map = json_decode(file_get_contents_from_url('https://geocode-maps.yandex.ru/1.x/?geocode='.$arProps[ADRES][VALUE].'&format=json&results=1'));
$center = $map->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos; 
$resmap = explode(" ", $center); 
$maps[0] = $resmap[1];
$maps[1] = $resmap[0];
}else{
$maps = explode(",", $arProps[MAPS][VALUE]);	
}
 ?>
 <div class="content_catalog">

 
 <div class="col-sm-3 col-xs-12 cart-detail-left" id="db-items">
  <?if($arFields["PREVIEW_PICTURE"]==true){?>
<div class="row">
<center>
    <div class="mag feed-com-img-wrap">
     <img style="cursor: zoom-in;" width="100%"
            onload="this.parentNode.className='feed-com-img-wrap';"
            src="<?= CFile::GetPath($arFields["PREVIEW_PICTURE"])?>"
            data-bx-viewer="image" 
            data-bx-title="<?=$ar_res['NAME']?>" 
            data-bx-src="<?= CFile::GetPath($arFields["PREVIEW_PICTURE"])?>" 
            data-bx-download="<?= CFile::GetPath($arFields["PREVIEW_PICTURE"])?>" 
            data-bx-width="1024" 
            data-bx-height="700" 
	  >
    </div>
	 <img src="/catalog/img/ruka.png" >
	
</center>
</div>
<?}?>
</div>
	
 <div class="col-sm-6 col-xs-12 cart-detail">
 
 <h2><?=$arProps[MAG][VALUE]?></h2>
 <div>
 <p><b>Юридическое лицо:</b>
 <?=$arFields['NAME']?>
 </p>
 <p><b>Адрес:</b>
 <?=$arProps[ADRES][VALUE]?>
 </p>
 <p><b>Тип дилера:</b>
 <ul>
 <?foreach($arProps[TYPE][VALUE] as $rec_dil){?>
 <li><?=$rec_dil?></li>
 <?}?>
 </ul>
 </p>
 <p><b>Телефоны:</b>
 <p><?=$arProps[PHONE][VALUE]?></p>
 <?for ($d = 0; $d <= 4; $d++) {?>
 <p><?=$arProps[PHONE]['VALUE'.$d]?></p>
 <?}?>

 </p>
 
 <p><b>Режим работы:</b>
 <p><?=$arProps[RABPNPT][VALUE]?></p>
 <p><?=$arProps[RABSB][VALUE]?></p>
 <p><?=$arProps[RABVS][VALUE]?></p>
 </p>

 <p><b>Сайт:</b>
 <a href="http://<?=$arProps[SITE][VALUE]?>"><?=$arProps[SITE][VALUE]?></a>
 </p>
 
   <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">
    </script>


 <script type="text/javascript">
        ymaps.ready(init);
      
        function init(){ 
            var myMap = new ymaps.Map("map", {
                center: [<?=$maps[0]?>, <?=$maps[1]?>],
                zoom: 17
            }); 
            
            var myPlacemark = new ymaps.Placemark([<?=$maps[0]?>, <?=$maps[1]?>], {
                hintContent: '<?=$arProps[MAG][VALUE]?>',
                balloonContent: '<?=$arProps[ADRES][VALUE]?>'
            });
            
            myMap.geoObjects.add(myPlacemark);
        }
    </script>
	
	 <div id="map" style="width: 500px; height: 500px"></div>
 
 </div>
 </div>
 </div>
 <script>
 BX.ready(function(){
   var obImageView = BX.viewElementBind(
      'db-items',
      {showTitle: true, lockScroll: false},
      function(node){
         return BX.type.isElementNode(node) && (node.getAttribute('data-bx-viewer') || node.getAttribute('data-bx-image'));
      }
   );
});
 </script>
<?}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>