 <?
function file_get_contents_from_url($url){

    $data = file_get_contents($url);

    if ($data === false){

        if (function_exists('curl_init')){

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            $data = curl_exec($curl);
            curl_close($curl);

        }

    }

    return $data;

}
$name = $_POST[cit]; 

$response = json_decode(file_get_contents_from_url('https://geocode-maps.yandex.ru/1.x/?geocode='.$name.'&format=json&results=1')); 


  
if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) 
{ 
   $center = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos; 
   $center = explode(" ", $center);
   $zoom = '10';
} 
else 
{ 
    $center[0] = '83.64';
	$center[1] = '50.76';
	$zoom = '4';
};

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(CModule::IncludeModule("iblock"))
?>
 <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
 <script type="text/javascript">
ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map('map', {
            center: [<?=$center[1]?>, <?=$center[0]?>],
            zoom: <?=$zoom?>
        }, {
            searchControlProvider: 'yandex#search'
        }),
        objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32,
            clusterDisableClickZoom: true
        });
		myMap.controls.remove('searchControl');

    // Чтобы задать опции одиночным объектам и кластерам,
    // обратимся к дочерним коллекциям ObjectManager.
    objectManager.objects.options.set('preset', 'islands#greenDotIcon');
    objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
    myMap.geoObjects.add(objectManager);
	<?if($_POST[typee] == false && $_POST[typeq]==false && $_POST[typew]==false && $_POST[typee]==false && $_POST[typer]==false){?>
	
	<?}else{?>
	objectManager.setFilter('properties.type == "<?if($_POST[type]){?>Территориальный дистрибьютор<?}?>" || properties.type2 == "<?if($_POST[typeq]){?>Розничный магазин<?}?>" || properties.type3 == "<?if($_POST[typew]){?>Оптовые продажи<?}?>" || properties.type4 == "<?if($_POST[typee]){?>Интернет-магазин<?}?>" || properties.type5 == "<?if($_POST[typer]){?>Розничная сеть<?}?>"');
	<?}?>
    $.ajax({
        url: "data.json"
    }).done(function(data) {
        objectManager.add(data);
    });
	
	

}
    </script> 

	 <div id="map" style="width: 100%; height: 942px"></div>
	 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>