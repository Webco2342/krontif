<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if(CModule::IncludeModule("iblock"))
$dillers = array();
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>6, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
if($_GET['REG']){
$APPLICATION->SetTitle($_GET['REG']);
$APPLICATION->AddChainItem($_GET['REG'], " ");
$arFilter[PROPERTY_REG] = $_GET['REG'];
}elseif($_GET['OKR']){
$arFilter[PROPERTY_OKR_VALUE] = $_GET['OKR'];
$arFilter[PROPERTY_TYPE] = 'Территориальный дистрибьютор';
$APPLICATION->SetTitle($_GET['OKR']);
$APPLICATION->AddChainItem("Наши территориальные Дистрибьюторы", "/partners/get/");
$APPLICATION->AddChainItem($_GET['OKR']." федеральный округ", " ");
}

$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1000), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
 ?>
 <div class="col-sm-6 col-xs-12">
 <h3><?=$arFields['NAME']?></h3>
 <div>
 <p><?=$arProps[ADRES][VALUE]."<br>".$arProps[PHONE][VALUE]?></p>
 <a href='/buy/detail.php?id=<?=$arFields[ID]?>'>Подробнее</a>
 </div>
 </div>
<?}


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>