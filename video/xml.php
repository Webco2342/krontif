<?
class ArrayToXML
{
	/**
	 * Функция конвертации массива в XML объект
	 * На вход подается мульти вложенный массив, на выходе получается с помощью рекурсии валидный xml
	 *
	 * @param array $data
	 * @param string $rootNodeName - корень вашего xml.
	 * @param SimpleXMLElement $xml - используется рекурсивно
	 * @return string XML
	 */
	public static function toXml($data, $rootNodeName = 'data', $xml=null)
	{
		// включить режим совместимости, не совсем понял зачем это но лучше делать
		if (ini_get('zend.ze1_compatibility_mode') == 1)
		{
			ini_set ('zend.ze1_compatibility_mode', 0);
		}
 
		if ($xml == null)
		{
			$xml = simplexml_load_string("<?xml version=\"1.0\" encoding=\"utf-8\"?><$rootNodeName />");
		}
 
		//цикл перебора массива 
		foreach($data as $key => $value)
		{
			// нельзя применять числовое название полей в XML
			if (is_numeric($key))
			{
				// поэтому делаем их строковыми
				$key = "unknownNode_". (string) $key;
			}
 
			// удаляем не латинские символы
			$key = preg_replace('/[^a-z0-9]/i', '', $key);
 
			// если значение массива также является массивом то вызываем себя рекурсивно
			if (is_array($value))
			{
				$node = $xml->addChild($key);
				// рекурсивный вызов
				ArrayToXML::toXml($value, $rootNodeName, $node);
			}
			else 
			{
				// добавляем один узел
                                $value = htmlentities($value);
				$xml->addChild($key,$value);
			}
 
		}
		// возвратим обратно в виде строки  или просто XML-объект 
		return $xml->asXML();
	}
}

$file = $_SERVER["DOCUMENT_ROOT"].'/video/token.txt';
$file_pointer = 'cam_off_time.txt';
$file_pointer_2 = 'log/'.date('Y.m.d').'.csv';

$myarray = array();
// Открываем файл для получения существующего содержимого
$current = file_get_contents($file);


if($_GET[refresh_token]){
	$refresh_token = $_GET[refresh_token];
}
elseif($current){
	$refresh_token = $current;
}
if($refresh_token){


	
$request_datar = array(
    'client_id' => 'metholding', 
    'client_secret' => '', 
	'grant_type' => 'refresh_token',
	'refresh_token' => $refresh_token, 

);

//Настраиваем cURL
$chr = curl_init();

curl_setopt($chr, CURLOPT_URL, 'https://api.ivideon.com/auth/oauth/token');
curl_setopt($chr, CURLOPT_FRESH_CONNECT, 1);
curl_setopt($chr, CURLOPT_HEADER, 0);
curl_setopt($chr, CURLOPT_POST, 1);
curl_setopt($chr, CURLOPT_POSTFIELDS, $request_datar);
curl_setopt($chr, CURLOPT_RETURNTRANSFER, true);

//Получаем данные
$responser = curl_exec($chr);
$my_new_arrayr = json_decode($responser, true); 
$token = $my_new_arrayr['access_token'];

$wait=exec('rm -r -f '.$_SERVER["DOCUMENT_ROOT"].'/video/token.txt');
file_put_contents($_SERVER["DOCUMENT_ROOT"].'/video/token.txt', $my_new_arrayr['refresh_token']);
}
/*--------------------------------------*/

if(!$token){

$request_data = array(
    'client_id' => 'metholding', 
    'client_secret' => '', 
	'grant_type' => 'authorization_code',
	'code' => $_GET[code], 

);

//Настраиваем cURL
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api.ivideon.com/auth/oauth/token');
curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $request_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//Получаем данные
$response = curl_exec($ch);
$my_new_array = json_decode($response, true); 
$token= $my_new_array['access_token'];

echo '<pre style="background-color: #f4f8fa; padding:20px; width:700px; text-align:left">';
print_r($my_new_array);
echo '</pre>';

$wait2=exec('rm -r -f '.$_SERVER["DOCUMENT_ROOT"].'/video/token.txt');
file_put_contents($_SERVER["DOCUMENT_ROOT"].'/video/token.txt', $my_new_arrayr['refresh_token']);

}

$chkr = curl_init();
curl_setopt($chkr, CURLOPT_URL, 'https://openapi-alpha-eu01.ivideon.com/cameras?op=FIND&access_token='.$token);
curl_setopt($chkr, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($chkr, CURLOPT_HEADER, 0);
curl_setopt($chkr, CURLOPT_POST, 1);

curl_setopt($chkr, CURLOPT_RETURNTRANSFER, true);
curl_setopt($chkr, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

$responsekr = curl_exec($chkr);
curl_close($chkr);

$my_new_arraykr = json_decode($responsekr, true);
foreach ($my_new_arraykr as $result_cam){
echo '<pre style="background-color: #f4f8fa; padding:20px; width:700px; text-align:left">';
print_r($my_new_arraykr);
echo '</pre>';
$myarray[] = array('ID' => $result_id["device_id"], 'NAME' => $my_new_arrayk[result][name], 'STATUS' => $result_id["type"], 'DATE' =>  $date);
}
$chke = curl_init();
curl_setopt($chke, CURLOPT_URL, 'https://openapi-alpha-eu01.ivideon.com/events?op=FIND&access_token='.$token);
curl_setopt($chke, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($chke, CURLOPT_HEADER, 0);
curl_setopt($chke, CURLOPT_POST, 1);
curl_setopt($chke, CURLOPT_RETURNTRANSFER, true);
curl_setopt($chke, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

$responseke = curl_exec($chke);
curl_close($chke);
$my_new_arrayke = json_decode($responseke, true); 

$current_time = file_get_contents($file_pointer);

foreach ($my_new_arrayke[result][items] as $result_id){
	

if($result_id["type"]=='status/offline'){

$chk = curl_init();
curl_setopt($chk, CURLOPT_URL, 'https://openapi-alpha-eu01.ivideon.com/cameras/'.$result_id["device_id"].'?op=GET&access_token='.$token);
curl_setopt($chk, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($chk, CURLOPT_HEADER, 0);
curl_setopt($chk, CURLOPT_POST, 1);

curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
curl_setopt($chk, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

$responsek = curl_exec($chk);
curl_close($chk);

$my_new_arrayk = json_decode($responsek, true); 
$ts = $result_id["time"];
$current_id = explode("|", $current_time);
$date = date('Y-m-d H:i:s', $ts);


$myarray[] = array('ID' => $result_id["device_id"], 'NAME' => $my_new_arrayk[result][name], 'STATUS' => $result_id["type"], 'DATE' =>  $date);
}
}

 
$data = array(
    'val1' => 111,
    'val2' => '222',
    'val3' => 333,
    500,
    'container' => array(
        'mystr' => 'test test',
        'myobj' => array(
            'x' => 250,
            'y' => 150,
            'name' => 'objName'
        )
    )
);
 
//echo ArrayToXML::toXML($myarray);

?>





