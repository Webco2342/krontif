<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Форма оформления заказа");
$APPLICATION->AddChainItem("Форма оформления заказа - ".$_GET[name_2]." ".$_GET[name], " ");
if(CModule::IncludeModule("iblock"))
?>
	<div class="col-md-2"></div>
			<div class="col-md-8">
				<form class="form-horizontal" role="form"  method="POST" id="registrationkrontif" action="javascript:void(null);" onsubmit="call()">
					<div class="form-group">
						<input type="hidden" name="zakaz" class="col-xs-12" value="<?=$_GET[name_2]?> <?=$_GET[name]?>" required>
					</div>					
					<div class="form-group">
					<p>Ваше имя: *</p>
						<input type="text" name="name" class="col-xs-12" placeholder="Ваше имя" required>
					</div>						
					<div class="form-group">
					<p>Телефон: *</p>
						<input type="text" name="phone" id="mtel" class="col-xs-12" placeholder="Телефон" required>
					</div>
					<div class="form-group">
					<p>Город: *</p>
						<input type="text" name="cit" id="cit" autocomplete="off" class="col-xs-12" placeholder="Город" required role="textbox" aria-autocomplete="list" aria-haspopup="true">
					</div>					
					<div class="form-group">
					 * Поля обязательные для заполнения
					 </div>
					<div class="form-group">
						<button type="submit" class="btn btn-danger">Отправить</button>
					</div>
<div id="results">
</div>
				</form>		


				<script type="text/javascript" language="javascript">
					function call() {
					  var msg   = $('#registrationkrontif').serialize();
						$.ajax({
						  type: 'POST',
						  url: 'zakaz-ajax.php',
						  data: msg,
						  success: function(data) {
							$('#results').html(data);
						  },
						  error:  function(xhr, str){
						alert('Возникла ошибка: ' + xhr.responseCode);
						  }
						});
						}
					
				</script>
				<script>
							window.onload = function() {
										var availableReg = [

									<? 

									 $SectList = CIBlockSection::GetList($arSort, array("IBLOCK_CODE"=>'CITY', "SECTION_ID"=>"2075", "ACTIVE"=>"Y") ,false, array("ID","IBLOCK_ID","NAME"));
											while ($SectListGet = $SectList->GetNext())
											{
												echo "'".$SectListGet['NAME']."', ";
											}   

									?>
									];
									jQuery( "#region" ).autocomplete({source: availableReg});

										var availableCity = [

									<?
									$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
									$arFilter = Array("IBLOCK_CODE"=>'CITY', "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
									$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5000), $arSelect);
									while($ob = $res->GetNextElement())
									{
									 $arFields = $ob->GetFields();
									 echo "'".$arFields['NAME']."', ";
									}
									?> 
										];
									jQuery( "#cit" ).autocomplete({source: availableCity});
								}				
			</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>