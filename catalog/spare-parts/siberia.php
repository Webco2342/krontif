<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Запасные части к котлам Сибирь");
$APPLICATION->AddChainItem("Сибирь", " ");
if(CModule::IncludeModule("iblock"))
?>

<div class="row">
<div class="col-xs-1"></div>
<div class="col-xs-10">
<image src="/local/templates/krontif_new/images/shema-sibiria.jpg" width="100%" />
</div>
<div class="col-sm-2 col-xs-12"></div>
<div class="col-sm-8 col-xs-12">
<h3 class="title_spare">НАИМЕНОВАНИЕ</h3>
<?
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>7, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID"=>"2157");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
?>
 
 <div class="col-sm-8 col-xs-6 st_spare">
 <?=$arFields["NAME"]?>
 </div>
  <div class="col-sm-2 col-xs-3 st_spare">
 <?=$arProps["VES"]["VALUE"]?> кг.
 </div>
  <div class="col-sm-2 col-xs-3">
 <a type="submit" class="btn btn-catalog-list css_popup<?=$arFields["ID"]?>">ЗАКАЗАТЬ</a>
 </div>


<div id="hideBlock<?=$arFields["ID"]?>" style="display:none;">
			<div class="col-md-12">
   <h3>Форма оформления заказа </h3>
   </div>
   <div class="col-md-2"></div>
			<div class="col-md-8">
			
   <form class="form-horizontal" role="form"  method="POST" id="zakazkrontif<?=$arFields["ID"]?>" action="javascript:void(null);" onsubmit="call()">
					<div class="form-group">
						<input type="hidden" name="zakaz" id="zakaz" class="col-xs-12" value="Демидовъ <?=$arFields["NAME"]?>" required>
					</div>					
					<div class="form-group">
					<p>Ваше имя: *</p>
						<input type="text" name="name" id="name" class="col-xs-12" placeholder="Ваше имя" required>
					</div>						
					<div class="form-group">
					<p>Телефон: *</p>
						<input type="text" name="phone" id="mtel" class="col-xs-12" placeholder="Телефон" required>
					</div>
					<div class="form-group">
					<p>Город: *</p>
						<input type="text" name="cit" id="cit" autocomplete="off" class="col-xs-12" placeholder="Город" required role="textbox" aria-autocomplete="list" aria-haspopup="true">
					</div>					
					<div class="form-group">
					 * Поля обязательные для заполнения
					 </div>
					<div class="form-group">
						<div type="submit" id="zakazkrontifz<?=$arFields["ID"]?>" class="btn btn-danger">Отправить</div>
					</div>
</form>
<div id="resultsz<?=$arFields["ID"]?>">
</div></div></div>

				<script type="text/javascript" language="javascript">

					

   window.BXDEBUG = true;
BX.ready(function(){
   var oPopup = new BX.PopupWindow('call_feedback', window.body, {
      autoHide : true,
      offsetTop : 1,
      offsetLeft : 0,
      lightShadow : true,
      closeIcon : true,
      closeByEsc : true,
      overlay: {
       
      }
   });
   oPopup.setContent(BX('hideBlock<?=$arFields["ID"]?>'));
   BX.bindDelegate(
      document.body, 'click', {className: 'css_popup<?=$arFields["ID"]?>' },
         BX.proxy(function(e){
            if(!e)
               e = window.event;
            oPopup.show();
            return BX.PreventDefault(e);
         }, oPopup)
   );
   
   
});

					

jQuery('#zakazkrontifz<?=$arFields["ID"]?>').click(function(){
var zakaz;
var name;
var phone;
var cit;

zakaz = jQuery('#zakaz').val();
name = jQuery('#name').val();
phone = jQuery('#mtel').val();
cit = jQuery('#cit').val();

$.post(
  "/catalog/zakaz-ajax.php",
  {
	zakaz: zakaz,
    name: name,
    phone: phone,
	cit: cit,

  },
  onAjaxSuccess
);
 
function onAjaxSuccess(data)
{  

$('#resultsz<?=$arFields["ID"]?>').html(data);
						  
}

					}
						);
				
					
				</script>
 <?

}
?>
* внешний диаметр
</div>


</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>