<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Каталог");
if(CModule::IncludeModule("iblock"))
?>
<div class="col-xs-1"><a name="catalog"></a></div>
<div class="col-xs-10">
<?
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>7, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID"=>array("2155","2154"));
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
 ?> 
<div class="col-xs-4 cart">
<div class="marcet_block">
<div class="tr"></div>
 <img src="<?= CFile::GetPath($arFields["PREVIEW_PICTURE"])?>">
 <a href="/catalog/detail.php?id=<?=$arFields["ID"]?>"> <?=$arFields["NAME"]?> </a>
 <span><?=$arProps["CENA"]["VALUE"]?> руб.</span>
 <a href="/catalog/detail.php?id=<?=$arFields["ID"]?>" type="submit" class="btn btn-catalog-list">Подробнее</a>
 <div class="plo"> <p>от <?=$arProps["PLO_1"]["VALUE"]?> м<sup>2</sup></p><p>до <?=$arProps["PLO_2"]["VALUE"]?> м<sup>2</sup> </p> </div>
 
</div>

</div>  
<?
}?>
</div>
<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>