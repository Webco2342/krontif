<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Твердотопливные котлы серия \"Сибирь\"");
if(CModule::IncludeModule("iblock"))
	 CJSCore::Init(Array("viewer"));
?>
<script src="/catalog/js/jquery.loupe.js" type="text/javascript"></script>
<center>
<ul class="catalog-menu nav navbar-nav">

<li>
<a href="advantages.php">
<div><img src="/catalog/img/ico1.png"></div>
<div>Преимущества</div>
</a>
</li>

<li>
<a href="ustroystvo.php">
<div><img src="/catalog/img/ico2.png"></div>
<div>Устройство и 
принцип действия</div>
</a>
</li>

<li>
<a href="teh.php">
<div><img src="/catalog/img/ico3.png"></div>
<div>Технические 
характеристики</div>
</a>
</li>

<li>
<a href="downloads.php">
<div><img src="/catalog/img/ico4.png"></div>
<div>Техническая
документация
скачать</div>
</a>
</li>

<li>
<a href="gabaritnye-razmery-kotlov.php">
<div><img src="/catalog/img/ico5.png"></div>
<div>Габаритные 
размеры котлов</div>
</a>
</li>
		
</ul>
</center>
<div class="content_catalog">
<ul class="nav nav-tabs" id="myTab">
  <li><a href="#catalog" data-toggle="tab">Каталог</a></li>
  <li class="active"><a href="#home" data-toggle="tab">Описание</a></li>

</ul>
<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="col-sm-4 col-xs-12">
<div id="db-items" >
    <div class="mag feed-com-img-wrap">
     <img  width="90%" src="/catalog/img/demidov.png"
			data-bx-viewer="image" 
            data-bx-title="Твердотопливные котлы серия Сибирь" 
            data-bx-src="/catalog/img/demidov.png" 
            data-bx-download="/catalog/img/demidov.png" 
            data-bx-width="1024" 
            data-bx-height="700" 
	 >
    </div>
	<div class="mag feed-com-img-wrap">
     <img  width="90%" src="/catalog/img/shema.png" 
	 	     data-bx-viewer="image" 
			 data-bx-title="Твердотопливные котлы серия Сибирь" 
			 data-bx-src="img_siberia.jpg" 
			 data-bx-download="img_siberia.jpg" 
			 data-bx-width="1024" 
			 data-bx-height="700" 
	 >
    </div>
</div>
	<div class="vbrc">
     <img src="/catalog/img/ico6.png" > <a href="/information/%20articles/">Выбрать котел</a>
    </div>

</div>

<div class="col-sm-8 col-xs-12">
<h2 style="color:#ed2e3e; font-size:20px;text-transform: uppercase;">Котлы KRONTIF серия «СИБИРЬ»</h2>

<p>Котлы чугунные одноконтурные, работающие на твердом топливе, под торговой маркой KRONTIF серии «СИБИРЬ» предназначены для отопления индивидуальных жилых домов и зданий коммунально-бытового назначения, оборудованных системами водяного отопления как с принудительной (закрытого типа), так и с естественной циркуляцией ( закрытого и открытого типа), с максимальной рабочей температурой 95℃ и рабочим давлением до 4 баррелей.</p>

<div style="color:#ed2e3e; font-size:20px; margin:20px 0">     <img src="/consumers/calculator/vn.png">     ВНИМАНИЕ! </div>
<p>Котлы, количество секций которых больше 6, рекомендованы только для систем с принудительной циркуляцией.</p>

<p>Котлы могут использоваться как основной источник тепловой энергии и как дополнение к существующим системам отопления с газовыми или электрическими котлами в качестве резервного.</p>

<p>В качестве топлива применяются уголь, а также дрова, кокс, брикеты и древесные отходы, т.е котлы всеядны и экономичны.</p>

<p><b style="color:#ed2e3e;">В отличие от серии «ДЕМИДОВЪ» котлы «СИБИРЬ» имеют:</b></p>
<ul class="krontif_ul">
    <li>Меньшее количество секций при аналогичной мощности c котлами «ДЕМИДОВЪ», что уменьшает габаритные размеры котла; </li>

    <li> Из-за меньшего кол-ва секций, топочная камера имеет меньший размер, поэтому котлы серии «Сибирь» (особенно маленьких мощностей) идеальны для отопления углем;</li>

     <li>Более широкий мощностной ряд – от 21 кВт до 80 кВт.</li>
</ul>
<p>
Рекомендован к применению тем собственникам, для которых важны: стоимость владения (цена на котел, оптимизация расходов на топливо и срок владения), надежность конструкции, неприхотливость.
</p>

              

</div>
<div class="col-xs-12 ">
<h2 style="color:#ed2e3e; font-size:20px;text-transform: uppercase;">ОСОБЕННОСТИ</h2>
<ul class="krontif_ul_2">
<li>тепловая мощность от 21 до 80 кВт</li>
<li>до 8 часов беспрерывной работы в режиме тления угля;</li>
<li>неприхотливость к качеству топлива, «всеядность»</li>
<li>высокая коррозионная стойкость;</li>
<li>при увеличении площади оттапливаемого помещения можно нарастить мощность, добавив секцию, а не покупать новый котел;</li>
<li>простая конструкция – нечему ломаться;</li>
<li>оптимальный КПД для данного типа котлов – до 83%</li>
<li>высокая ремонтопригодность по сравнению со стальными аналогами;</li>
<li>водоохлаждаемые колосники, что обеспечивает долгий срок их службы;</li>
<li>в комплект включен термостатический тягорегулятор;</li>
<li>оснащен термометром для контроля температуры теплоносителя;</li>
</ul>
</div>
</div>

<div class="tab-pane" id="catalog">
<div class="col-sm-1 col-xs-12" ><a name="catalog"></a></div>
<div class="col-sm-10 col-xs-12">

<?
$activeElements = CIBlockSection::GetSectionElementsCount("2154", Array("CNT_ACTIVE"=>"Y"));
if($activeElements == 4 || $activeElements == 7 || $activeElements == 10 || $activeElements == 13){
$d = $activeElements;
}else{
$d =  false;
}
$i = 1;
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>7, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID"=>"2154");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arProps = $ob->GetProperties();
 if($i == $d){?>
<div class="col-sm-4 col-xs-12 cart">
</div> 
 <?}?> 
<div class="col-sm-4 col-xs-12 cart">
<div class="marcet_block">
<div class="tr"></div>
 <img src="<?= CFile::GetPath($arFields["PREVIEW_PICTURE"])?>">
 <a href="/catalog/detail.php?id=<?=$arFields["ID"]?>"> <?=$arFields["NAME"]?> </a>
 <span><?=$arProps["CENA"]["VALUE"]?> руб.</span>
 <a href="/catalog/detail.php?id=<?=$arFields["ID"]?>" type="submit" class="btn btn-catalog-list">Подробнее</a>
 <div class="plo"> <p>от <?=$arProps["PLO_1"]["VALUE"]?> м<sup>2</sup></p><p>до <?=$arProps["PLO_2"]["VALUE"]?> м<sup>2</sup> </p> </div>
 
</div>

</div>
<?
$i++;
}
?>
 				<script type="text/javascript" language="javascript">
				$(".loop").loupe({
						width: 300,     // ширина лупы
						height: 300,    // высота лупы
						loupe: "loupe" // css класс лупы
					});
										BX.ready(function(){
   var obImageView = BX.viewElementBind(
      'db-items',
      {showTitle: true, lockScroll: false},
      function(node){
         return BX.type.isElementNode(node) && (node.getAttribute('data-bx-viewer') || node.getAttribute('data-bx-image'));
      }
   );
});
	
				</script>

</div>
</div></div>
<script>
  $(function () {
    $('#myTab a:last').tab('show')
  })
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>