<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Устройство и  принцип действия");
$APPLICATION->AddChainItem("Устройство и  принцип действия&quot;", " ");
CJSCore::Init(Array("viewer"));
?>
<span id="db-items">
<center>
<ul class="catalog-menu nav navbar-nav">

<li >
<a href="advantages.php">
<div><img src="/catalog/img/ico1.png"></div>
<div>Преимущества</div>
</a>
</li>

<li class="active">
<a href="ustroystvo.php">
<div><img src="/catalog/img/ico2.png"></div>
<div>Устройство и 
принцип действия</div>
</a>
</li>

<li>
<a href="teh.php">
<div><img src="/catalog/img/ico3.png"></div>
<div>Технические 
характеристики</div>
</a>
</li>

<li>
<a href="downloads.php">
<div><img src="/catalog/img/ico4.png"></div>
<div>Техническая
документация
скачать</div>
</a>
</li>

<li>
<a href="gabaritnye-razmery-kotlov.php">
<div><img src="/catalog/img/ico5.png"></div>
<div>Габаритные 
размеры котлов</div>
</a>
</li>
		
</ul>
</center>
<div class="cal_bloc">
<div class="col-xs-5">
<img style="width:100%" onload="this.parentNode.className='feed-com-img-wrap';"
            src="/catalog/img/ustroistvo.png"
            data-bx-viewer="image" 
            data-bx-title="Основные части котла" 
            data-bx-src="/catalog/img/ustroistvobig.jpg" 
            data-bx-width="1040" 
            data-bx-height="800">
</div>
<div class="col-xs-7 bclock_red_contur">
Главной частью котла является ЧУГУННЫЙ СЕКЦИОННЫЙ ТЕПЛООБМЕННИК, который также является и самым уязвимым местом у котлов. И хотя чугунные изделия являются классикой и символом долговечности и эффективности, некачественное сырье и плохо отработанная технология литья могут стать одними из причин разрушения теплообменника, что делает котел непригодным для дальнейшей эксплуатации. 
</div>
</div>
<div class="cal_bloc col-xs-12">
<p>Для производства котлов <b>KRONTIF завод АО «Кронтиф-центр»</b> использует серый высококачественный чугун собственного производства, а 280-ти летний опыт в литейном деле, позволяет заводу  изготавливать высокотехнологичные и надежные чугунные секции.</p>

<p><b>Теплообменник котла состоит из секций 3 видов:</b> передней, задней и средней, соединенных между собой запрессованными ниппелями и скрепленными стяжными шпильками. Между передней и задней секциями находятся от 1 до 6 средних секций (одного типа), которые делят котел на камеру сгорания (топка) и зольное пространство, конвекционную часть и водяное пространство (внутри секций).</p>

<p>В задней секции котла в верхней её части находится патрубок дымохода и фланец для отвода нагретого теплоносителя (отопительной воды) в систему отопления, в нижней части имеется фланец для подвода обратной воды из системы отопления.</p>

<p style="font-style: italic;">
К передней секции прикреплены крышка для чистки конвективных поверхностей дымохода (дымовые каналы), загрузочная дверца, шуровочная дверца и поддувальная дверца. В верхней части секции размещена сквозная заглушка, в которую устанавливается термостатический регулятор тяги.
</p>
</div>
<div class="cal_bloc">
<div class="col-xs-7 bclock_red_contur bclock_red_v2">
Необходимая мощность котла подбирается количеством секций. Если в процессе эксплуатации оборудования, увеличится отапливаемая площадь помещения (пристройка к дому, адаптация чердачного помещения в жилое и т.д.), не нужно будет покупать новый котел с подходящей мощностью. Просто добавьте секции в существующий.  
</div>
<div style="float:right;" class="col-xs-5">
<img style="width:100%" onload="this.parentNode.className='feed-com-img-wrap';"
            src="/catalog/img/ustroistvo2.png"
            data-bx-viewer="image" 
            data-bx-title="Основные части котла" 
            data-bx-src="/catalog/img/ustroistvobig2.jpg" 
            data-bx-width="1040" 
            data-bx-height="800">
</div>
</div>
<div class="cal_bloc col-xs-12">
<p>
Теплообменник котла изолирован безвредной для здоровья минеральной изоляцией, которая снижает потери тепла в окружающую среду и обшит металлическим кожухом (сверху и с боков). Металлическая обшивка покрыта качественной краской.
</p>
<p>
<b>К СВЕДЕНИЮ!</b> Материалы и покрытия, применяемые для изготовления котлов, применяются из числа разрешенных Федеральной службой по надзору в сфере защиты прав потребителей и благополучия человека РФ.
</p>
    <div style="color:#ed2e3e; font-size:20px;">     <img src="/consumers/calculator/vn.png">     ВНИМАНИЕ! </div> 
<p>
Для правильной работы котла и его экономичной эксплуатации важно, чтобы его номинальная мощность соответствовала потерям тепла отапливаемых помещений. Поэтому для правильного подбора котла оптимальной мощности для Вашего дома обращайтесь к квалифицированным специалистам.
</p>
</div>
</span>
<script>
BX.ready(function(){
   var obImageView = BX.viewElementBind(
      'db-items',
      {showTitle: true, lockScroll: false},
      function(node){
         return BX.type.isElementNode(node) && (node.getAttribute('data-bx-viewer') || node.getAttribute('data-bx-image'));
      }
   );
});
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>