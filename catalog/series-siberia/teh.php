<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ТЕХНИЧЕСКИЕ ХАРАКТЕРИСТИКИ");
$APPLICATION->AddChainItem("Технические характеристики", " ");
?>

<center>
<ul class="catalog-menu nav navbar-nav">

<li >
<a href="advantages.php">
<div><img src="/catalog/img/ico1.png"></div>
<div>Преимущества</div>
</a>
</li>

<li >
<a href="ustroystvo.php">
<div><img src="/catalog/img/ico2.png"></div>
<div>Устройство и 
принцип действия</div>
</a>
</li>

<li class="active">
<a href="teh.php">
<div><img src="/catalog/img/ico3.png"></div>
<div>Технические 
характеристики</div>
</a>
</li>

<li>
<a href="downloads.php">
<div><img src="/catalog/img/ico4.png"></div>
<div>Техническая
документация
скачать</div>
</a>
</li>

<li>
<a href="gabaritnye-razmery-kotlov.php">
<div><img src="/catalog/img/ico5.png"></div>
<div>Габаритные 
размеры котлов</div>
</a>
</li>
		
</ul>
</center>
<div class="cal_bloc">
<div class="table-responsive"> 
<table class="table table-bordered table-hover table-striped">

<thead class="thead">
<tr>
	<th colspan="2" rowspan="2">
		
			Обозначение
		
	</th>
	<th colspan="8" class="thl">
		
			СИБИРЬ
		
	</th>
</tr>
<tr>
	<th class="th thl">
		
			 21
		
	</th>
	<th class="th thl">
		
			 30
		
	</th>
	<th class="th thl">
		
			 40
		
	</th>
	<th colspan="2" class="th thl">
		
			 50
		
	</th>
	<th class="th thl">
	
			 60
		
	</th>
	<th class="th thl">
		
			 70
		
	</th>
	<th class="th thl">
		
			 80
		
	</th>
</tr>
</thead>
<tbody>
<tr>
	<td>
			 Количество секций
	</td>
	<td>
			 шт.
	</td>
	<td>
			 3
	</td>
	<td>
			 4
	</td>
	<td>
			 5
	</td>
	<td colspan="2">
	
			 6
	</td>
	<td>
			 7
	</td>
	<td>
			 8
	</td>
	<td>
			 9
	</td>
</tr>

<tr>
	<td>
			Номинальная теплопроизводительность (антрацит)
	</td>
	<td>
			 кВт
	</td>
	<td>
			 21
	</td>
	<td>
			 30
	</td>
	<td>
			 40
	</td>
	<td colspan="2">
		
			 50
	</td>
	<td>
			 60
	</td>
	<td>
			 70
	</td>
	<td>
			 80
	</td>
</tr>

<tr>
	<td>
			 Номинальная теплопроизводительность (дрова)
	</td>
	<td>
			 кВт
	</td>
	<td>
			 16
	</td>
	<td>
			 24
	</td>
	<td>
			 32
	</td>
	<td colspan="2">
	
			 40
	</td>
	<td>
		
			 48
	</td>
	<td>
		
			 56
	</td>
	<td>
		
			 64
	</td>
</tr>

<tr>
	<td>
			 КПД/антрацит
	</td>
	<td>
			 %
	</td>
	<td colspan="8">
		<center>
			 78-80</center>
	</td>
</tr>

<tr>
	<td>
			 КПД/дрова*
	</td>
	<td>
			 %
	</td>
	<td colspan="8">
		<center>
			 69-75</center>
	</td>
</tr>

<tr>
	<td>
			 Максимальная температура воды в котле
	</td>
	<td>
°C
	</td>
	<td colspan="8">
		<center>
			 95</center>
	</td>
</tr>

<tr>
	<td>
			Минимальная температура в обратной линии**
	</td>
	<td>
			°C
	</td>
	<td colspan="8">
		<center>
			55</center>
	</td>	
</tr>

<tr>
	<td>
			Допустимая разница между температурами подающей линии и  обратной линии***
	</td>
	<td>
			°C
	</td>
	<td colspan="8">
		<center>
			не более 20</center>
	</td>	
</tr>

<tr>
	<td>
			 Время горения при номинальной теплопроизводительности (антрацит)
	</td>
	<td>
			 час
	</td>
	<td colspan="8">
		<center>
			 &gt;4,5</center>
	</td>
</tr>

<tr>
	<td>
			 Время сгорания/дрова
	</td>
	<td>
			 час
	</td>
	<td colspan="8">
		<center>
			 &gt;2</center>
	</td>
</tr>

<tr>
	<td>
			 Объем водяной полости котла
	</td>
	<td>
			л
	</td>
	<td>
			 24
	</td>
	<td>
			 30
	</td>
	<td>
			 36
	</td>
	<td colspan="2">
	
			 42
	</td>
	<td>
			48
	</td>
	<td>
			54
	</td>
	<td>
			 60
	</td>
</tr>

<tr>
	<td>
			 Макс. рабочее давление
	</td>
	<td>
бар
	</td>
	<td colspan="8">
		<center>
			 4</center>
	</td>
</tr>

<tr>
	<td>
			 Макс. длина деревянных поленьев
	</td>
	<td>
			 мм
	</td>
	<td>
			 195
	</td>
	<td>
			 320
	</td>
	<td>
			 445
	</td>
	<td colspan="2">
	
			 570
	</td>
	<td>
			 695
	</td>
	<td>
			 820
	</td>
	<td>
			 945
	</td>
</tr>

<tr>
	<td>
			 Диаметр подключения дымовой трубы<p></p>
	</td>
	<td>
			 мм
	</td>
	<td colspan="4">
		<center>
			 203</center>
	</td>
	<td colspan="4">
		<center>
			 203</center>
	</td>
</tr>

<tr>
	<td>
			 Требуемое разрежение в дымовой трубе (тяга), <br>не менее
	</td>
	<td>
			 Па
	</td>
	<td colspan="4">
		<center>
			 15</center>
	</td>
	<td colspan="4">
		<center>
			 25</center>
	</td>
</tr>

<tr>
	<td>
			 Температура дымовых газов, не более
	</td>
	<td>
	°C
	</td>
	<td colspan="8">
		<center>
			 250</center>
	</td>
</tr>

<tr>
	<td>
			 Вход/выход теплоносителя
	</td>
	<td>
			 дюйм
	</td>
	<td colspan="8">
		<center>
			 2</center>
	</td>
</tr>

<tr>
	<td>
			 Масса
	</td>
	<td>
			 кг
	</td>
	<td>
			 280
	</td>
	<td>
			 340
	</td>
	<td>
			 400
	</td>
	<td colspan="2">

			 460
	</td>
	<td>
			 520
	</td>
	<td>
			 580
	</td>
	<td>
			 640
	</td>
</tr>

<tr>
	<td>
			 Высота котла
	</td>
	<td>
			 мм
	</td>
	<td colspan="8">
		<center>
			 1130</center>
	</td>
</tr>

<tr>
	<td>
			Ширина котла
	</td>
	<td>
			 мм
	</td>
	<td colspan="8">
		<center>
			 536</center>
	</td>
</tr>

<tr>
	<td>
			 Глубина котла (L)
	</td>
	<td>
			 мм
	</td>
	<td>
			 420
	</td>
	<td>
			 543
	</td>
	<td>
			 666
	</td>
	<td colspan="2">

			 789
	</td>
	<td>
			 912
	</td>
	<td>
			 1035
	</td>
	<td>
			 1158
	</td>
</tr>

<tr>
	<td>
			 Размер топки (ширина, длина, высота)
	</td>
	<td>
			 мм
	</td>
	<td>
			 330/205/510
	</td>
	<td>
			 330/330/510
	</td>
	<td>
			330/455/510
	</td>
	<td colspan="2">

			 330/580/510
	</td>
	<td>
			330/705/510
	</td>
	<td>
			 330/830/510
	</td>
	<td>
			 330/955/510
	</td>
</tr>

<tr>
	<td>
			 Объем топки
	</td>
	<td>
			 л
	</td>
	<td>
			 34
	</td>
	<td>
			 55
	</td>
	<td>
			 76
	</td>
	<td colspan="2">
		
			 97
	</td>
	<td>
			 118

	</td>
	<td>

			 139

	</td>
	<td>

			 160

	</td>
</tr>

<tr>
	<td>
			 Объем аккумулирующего бака
	</td>
	<td>
			 л
	</td>
	<td>
			 540
	</td>
	<td>
			 800
	</td>
	<td>
			 1000
	</td>
	<td colspan="2">
		
			 1300
	</td>
	<td>
			 1495

	</td>
	<td>

			 1650

	</td>
	<td>

			 1805

	</td>
</tr>

<tr>
	<td>
			 Рекомендуемая площадь отопления(уголь) 
	</td>
	<td>
м2
	</td>
	<td>
			 от 120 до 160
	</td>
	<td>
			 от 161 до 240
	</td>
	<td>
			 от 241 до 340
	</td>
	<td colspan="2">
		
			от 341 до 440
	</td>
	<td>
			 от 441 до 540

	</td>
	<td>

			 от 541 до 640

	</td>
	<td>

			от 641 до 740

	</td>
</tr>

<tr>
	<td>
			 Рекомендуемая площадь отопления(дрова) 
	</td>
	<td>
м2
	</td>
	<td>
			 от 80 до 130
	</td>
	<td>
			 от 131 до 180
	</td>
	<td>
			от 181 до 260
	</td>
	<td colspan="2">
		
			от 261 до 340
	</td>
	<td>
			 от 341 до 420

	</td>
	<td>

			 от 421 до 500 

	</td>
	<td>

			 от 501 до 580

	</td>
</tr>


</tbody>
</table>	
<div class="col-xs-11">

<p>при условии влажности дров не более 20% </p>
<p>при более низкой температуре в топке образуется конденсат и смола, которые оказывают губительное действие на котел </p>
<p>при несоблюдении данного условия возникает опасность температурного шока, что приведет к разрушению чугунного теплообменника </p>

</div>	
<div style="text-align:right" class="col-xs-1">
<p>*</p>
<p>**</p>
<p>***</p>
</div>	
		</div></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>