<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("преимущества");
$APPLICATION->AddChainItem("Преимущества&quot;", " ");
?>
<center>
<ul class="catalog-menu nav navbar-nav">

<li class="active">
<a href="advantages.php">
<div><img src="/catalog/img/ico1.png"></div>
<div>Преимущества</div>
</a>
</li>

<li>
<a href="ustroystvo.php">
<div><img src="/catalog/img/ico2.png"></div>
<div>Устройство и 
принцип действия</div>
</a>
</li>

<li>
<a href="teh.php">
<div><img src="/catalog/img/ico3.png"></div>
<div>Технические 
характеристики</div>
</a>
</li>

<li>
<a href="downloads.php">
<div><img src="/catalog/img/ico4.png"></div>
<div>Техническая
документация
скачать</div>
</a>
</li>

<li>
<a href="gabaritnye-razmery-kotlov.php">
<div><img src="/catalog/img/ico5.png"></div>
<div>Габаритные 
размеры котлов</div>
</a>
</li>
		
</ul>

<div class="st">

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Долговечность
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Бесперебойная работа котла гарантирована не менее, чем 30 лет, благодаря теплообменнику из высококачественного чугуна собственного производства; 

			</div>
		</div>
	</div>
</div>

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 250 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="250" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Износостойкость
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Высокая устойчивость чугуна к коррозии и к высоким температурам по сравнению со стальными аналогами. 

			</div>
		</div>
	</div>
</div>

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Автономность
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Независимость от электричества при установке котла в систему с естественной циркуляцией теплоносителя; (котлы до 6 секций)

			</div>
		</div>
	</div>
</div>

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 250 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="250" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Технологичность
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Высокая гидравлическая прочность литого изделия по сравнению с конструкциями со сварными швами. Это снижает вероятность утечек и разрушения теплообменника. Высокая теплоемкость позволяет дольше 
сохранять тепло;

			</div>
		</div>
	</div>
</div>

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 400 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="400" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Высокая ремонтопригодность
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Секционность теплообменника позволяет с легкостью заменять секции. А физическая и финансовая доступность запасных частей по сравнению с иностранными аналогами, делает процесс замены деталей более быстрым и экономичным.

			</div>
		</div>
	</div>
</div>

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 500 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="500" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Удобство и простота в эксплуатации
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Большие размеры дверки позволяют быстро загрузить и разжечь дрова. Большая топочная камера позволяет использовать дрова длиной до 90см (в зависимости от модели);

			</div>
		</div>
	</div>
</div>

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Надежность
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Простота конструкции исключает вероятность серьезных поломок и перебоев в работе.
			</div>
		</div>
	</div>
</div>

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 220 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="220" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Эффективность
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Высокий КПД (до 83% в зависимости от вида топлива) достигается за счет метода литья, благодаря которому теплообменник имеет более сложную форму поверхности, чем у стальных аналогов, а также за счет качественной теплоизоляции и герметизации топки.
			</div>
		</div>
	</div>
</div>

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 250 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="250" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Неприхотливость
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Не требуют дымососа, «всеядны».
			</div>
		</div>
	</div>
</div>

<span class="content_h2_title">
<svg width="100%" height="65px" version="1.2" baseProfile="tiny"   x="0px" y="0px" xml:space="preserve" >	
<line fill="none" stroke="#aaa" x1="0" stroke-width="2px" y1="50%" x2="100%" y2="50%"/>
<svg  width="100%" height="100%" y="" viewBox="0 0 200 70" preserveAspectRatio="xMidYMid meet">
<rect r="0.5cm" cx="50%" cy="50%" fill="#fff" width="200" height="60" />
<text x="50%" y="50%" alignment-baseline="middle" text-anchor="middle" class="h2">
Разумная цена
</text>
</svg>
</svg>
</span> 
<div class="bg_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
Российская цена – европейское качество!
			</div>
		</div>
	</div>
</div>

</div>
</center>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>