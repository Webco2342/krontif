<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Габаритные  размеры котлов");
$APPLICATION->AddChainItem("Габаритные  размеры котлов", " ");
?>

<center>
<div class="cal_bloc">
<ul class="catalog-menu nav navbar-nav">

<li >
<a href="advantages.php">
<div><img src="/catalog/img/ico1.png"></div>
<div>Преимущества</div>
</a>
</li>

<li >
<a href="ustroystvo.php">
<div><img src="/catalog/img/ico2.png"></div>
<div>Устройство и 
принцип действия</div>
</a>
</li>

<li >
<a href="teh.php">
<div><img src="/catalog/img/ico3.png"></div>
<div>Технические 
характеристики</div>
</a>
</li>

<li >
<a href="downloads.php">
<div><img src="/catalog/img/ico4.png"></div>
<div>Техническая
документация
скачать</div>
</a>
</li>

<li class="active">
<a href="gabaritnye-razmery-kotlov.php">
<div><img src="/catalog/img/ico5.png"></div>
<div>Габаритные 
размеры котлов</div>
</a>
</li>
		
</ul>
</div>
<div class="cal_bloc">
<h3 style="text-transform: uppercase;">Габаритные и присоединительные размеры котлов «ДЕМИДОВЪ»</h3>

<img style="width:100%; max-width:900px" src="/catalog/img/ban3.png">
</div>
</center>
</div></div></div></div>
<div class="page">
	<div class="container">
		<div class="row">
			<div class="col-md-12 lenta">
<div class="table-responsive"> 
<table class="table table-bordered table-hover table-striped">
<thead>
<tr>
	<th colspan="2">
	
			 название модели
		
	</th>
	<th >
		
			 ДЕМИДОВЪ  15
		
	</th>
	<th >
		
			 ДЕМИДОВЪ 22
		
	</th>
	<th >
		
			 ДЕМИДОВЪ 28
		
	</th>
	<th >
		
			 ДЕМИДОВЪ 35
		
	</th>
	<th >
	
			 ДЕМИДОВЪ 42
		
	</th>
	<th >
		
			 ДЕМИДОВЪ 49
		
	</th>

</tr>
</thead>
<tbody>
<tr>
	<td>
			 размер
	</td>
	<td>
			 L/L1 (мм)
	</td>
	<td>
			645/335
	</td>
	<td>
			772/462
	</td>
	<td>
			 889/589
	</td>
	<td>
			 1026/716
	</td>
	<td>
	
			 1153/843
	</td>
	<td>
			 1280/716
	</td>


</tr>

</tbody>
</table>			
		</div></div>
</div></div>
<div class="lenta2"> </div>
</div>
<div class="bg_catalog_index_hit_clic">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<center>
<div class="cal_bloc">
<h3 style="text-transform: uppercase;">Габаритные размеры упаковки</h3>

<img style="width:100%; max-width:900px" src="/catalog/img/ban2.png">
</div>
</center>			
		</div></div>
</div></div>

</div></div></div></div>

<div class="page2 ">
	<div class="container">
		<div class="row">
			<div class="col-md-12 lenta">
<div class="table-responsive"> 
<table class="table table-bordered table-hover table-striped">
<thead>
<tr>
	<th colspan="2">
	
	
			 название модели
		
	</th>
	<th >
		
			 ДЕМИДОВЪ  15
		
	</th>
	<th >
		
			 ДЕМИДОВЪ 22
		
	</th>
	<th >
		
			 ДЕМИДОВЪ 28
		
	</th>
	<th >
		
			 ДЕМИДОВЪ 35
		
	</th>
	<th >
	
			 ДЕМИДОВЪ 42
		
	</th>
	<th >
		
			 ДЕМИДОВЪ 49
		
	</th>

</tr>
</thead>
<tbody>
<tr>
	<td>
			 размер
	</td>
	<td>
			 L/L1 (мм)
	</td>
	<td>
			880
	</td>
	<td>
			880
	</td>
	<td>
			 880
	</td>
	<td>
			 925
	</td>
	<td>
	
			1050
	</td>
	<td>
			1175
	</td>

</tr>

</tbody>
</table>			
		</div></div>
</div></div>
<div class="lenta3"> </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>