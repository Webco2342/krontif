<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ТЕХНИЧЕСКИЕ ХАРАКТЕРИСТИКИ");
$APPLICATION->AddChainItem("Технические характеристики", " ");
?>

<center>
<ul class="catalog-menu nav navbar-nav">

<li >
<a href="advantages.php">
<div><img src="/catalog/img/ico1.png"></div>
<div>Преимущества</div>
</a>
</li>

<li >
<a href="ustroystvo.php">
<div><img src="/catalog/img/ico2.png"></div>
<div>Устройство и 
принцип действия</div>
</a>
</li>

<li class="active">
<a href="teh.php">
<div><img src="/catalog/img/ico3.png"></div>
<div>Технические 
характеристики</div>
</a>
</li>

<li>
<a href="downloads.php">
<div><img src="/catalog/img/ico4.png"></div>
<div>Техническая
документация
скачать</div>
</a>
</li>

<li>
<a href="gabaritnye-razmery-kotlov.php">
<div><img src="/catalog/img/ico5.png"></div>
<div>Габаритные 
размеры котлов</div>
</a>
</li>
		
</ul>
</center>
<div class="cal_bloc">
<div class="table-responsive"> 
<table class="table table-bordered table-hover table-striped">

<thead class="thead">
<tr>
	<th colspan="2" rowspan="2">
		
			Обозначение
		
	</th>
	<th colspan="6">
		
			ДЕМИДОВЪ
		
	</th>
</tr>
<tr>
	<th class="th">
		
			ДЕМИДОВЪ 15
		
	</th>
	<th class="th">
		
			ДЕМИДОВЪ 22
		
	</th>
	<th class="th">
		
			ДЕМИДОВЪ 28
		
	</th>
	<th class="th">
		
			ДЕМИДОВЪ 35
		
	</th>
	<th class="th">
		
			ДЕМИДОВЪ 42
		
	</th>
	<th class="th">
	
			ДЕМИДОВЪ 49
		
	</th>
</tr>
</thead>
<tbody>
<tr>
	<td>
			Количество секций
	</td>
	<td>
			шт.
	</td>
	<td>
			3
	</td>
	<td>
			4
	</td>
	<td>
			5
	</td>
	<td>
			6
	</td>
	<td>
			7
	</td>
	<td>
			8
	</td>
</tr>
<tr>
	<td>
			Номинальная теплопроизводительность (уголь)
		

	</td>
	<td>
			кВт
	</td>
	<td>
			15
	</td>
	<td>
			22
	</td>
	<td>
			28
	</td>
	<td>
			35
	</td>
	<td>
			42
	</td>
	<td>
			49
	</td>
</tr>
<tr>
	<td>
			Номинальная теплопроизводительность (дрова)
		

	</td>
	<td>
кВт
	</td>
	<td>
			12
	</td>
	<td>
			19
	</td>
	<td>
			22,4
	</td>
	<td>
			28
	</td>
	<td>
			34
	</td>
	<td>
			39
	</td>
</tr>
<tr>
	<td>
			КПД/уголь
	</td>
	<td>
			%
	</td>
	<td colspan="6">
		<center>
			80-83</center>
	</td>
</tr>
<tr>
	<td>
			КПД/дрова*
	</td>
	<td>
			%
	</td>
	<td colspan="6">
		<center>
			71-78</center>
	</td>
</tr>
<tr>
	<td>
			Максимальная температура воды в котле
	</td>
	<td>
°C
	</td>
	<td colspan="6">
		<center>
			90</center>
	</td>
</tr>
<tr>
	<td>
			Минимальная температура в обратной линии**
	</td>
	<td>
			°C
	</td>
	<td colspan="6">
		<center>
			55</center>
	</td>	
</tr>
<tr>
	<td>
			Допустимая разница между температурами подающей линии и  обратной линии***
	</td>
	<td>
			°C
	</td>
	<td colspan="6">
		<center>
			не более 20</center>
	</td>	
</tr>


<tr>
	<td>
			Время горения при номинальной теплопроизводительности ( уголь)
	</td>
	<td>
			час
	</td>
	<td colspan="6">
		<center>
			&gt;4</center>
	</td>
</tr>
<tr>
	<td>
			Время сгорания/дрова,
	
			не менее
	</td>
	<td>
			час
	</td>
	<td colspan="6">
		<center>
			&gt;2</center>
	</td>
</tr>

<tr>
	<td>
			Объем водяной полости котла
	</td>
	<td>
			л
	</td>
	<td>
			22
	</td>
	<td>
			28
	</td>
	<td>
			34
	</td>
	<td>
			40
	</td>
	<td>
			46
	</td>
	<td>
			53
	</td>
</tr>

<tr>
	<td>
			Макс. рабочее давление
	</td>
	<td>
			бар
	</td>
	<td colspan="6">
		<center>
			4</center>
	</td>
</tr>

<tr>
	<td>
			Макс. длина деревянных поленьев
	</td>
	<td>
			мм
	</td>
	<td>
			245
	</td>
	<td>
			370
	</td>
	<td>
			495
	</td>
	<td>
			620
	</td>
	<td>
			745
	</td>
	<td>
			870
	</td>
</tr>

<tr>
	<td>
			Диаметр подключения дымовой трубы
		

	</td>
	<td>
			мм
	</td>
	<td colspan="3">
		<center>
			156</center>
	</td>
	<td colspan="3">
		<center>
			192</center>
	</td>
</tr>

<tr>
	<td>
			Требуемое разрежение в дымовой трубе (тяга), не менее
		

	</td>
	<td>
			Па
	</td>
	<td>
			0,02
	</td>
	<td>
			0,023
	</td>
	<td>
			0,025
	</td>
	<td>
			0,028
	</td>
	<td>
			0,029
	</td>
	<td>
			0,03
	</td>
</tr>

<tr>
	<td>
			Температура дымовых газов, не более
	</td>
	<td>
°C
	</td>
	<td colspan="6">
		<center>
			250</center>
	</td>
</tr>

<tr>
	<td>
			Вход/выход теплоносителя
	</td>
	<td>
			Ду, мм/резьба
	</td>
	<td colspan="6">
		<center>
			50/G2-B</center>
	</td>
</tr>

<tr>
	<td>
			Масса
	</td>
	<td>
			кг
	</td>
	<td>
			236,6
	</td>
	<td>
			289
	</td>
	<td>
			341,3
	</td>
	<td>
			393,7
	</td>
	<td>
			446
	</td>
	<td>
			498,4
	</td>
</tr>

<tr>
	<td>
			Высота котла
	</td>
	<td>
мм
	</td>
	<td colspan="6">
		<center>
			938</center>
	</td>
</tr>

<tr>
	<td>
			Ширина котла
	</td>
	<td>
мм
	</td>
	<td colspan="6">
		<center>
			487</center>
	</td>
</tr>

<tr>
	<td>
			Глубина котла (L)
		

	</td>
	<td>
			мм
	</td>
	<td>
			585
	</td>
	<td>
			710
	</td>
	<td>
			835
	</td>
	<td>
			960
	</td>
	<td>
			1085
	</td>
	<td>
			1210
	</td>
</tr>

<tr>
	<td>
			Размер топки (ширина, длина, высота)
		

	</td>
	<td>
			мм
	</td>
	<td>
			310/255/368
	</td>
	<td>
			310/380/368
	</td>
	<td>
			310/505/368
	</td>
	<td>
			310/630/368
	</td>
	<td>
			310/755/368
	</td>
	<td>
			310/880/368
	</td>
</tr>

<tr>
	<td>
			Объем топки
		

	</td>
	<td>
			л
	</td>
	<td>
			29
	</td>
	<td>
			43
	</td>
	<td>
			58
	</td>
	<td>
			72
	</td>
	<td>
			86
	</td>
	<td>
			100
	</td>
</tr>

<tr>
	<td>
			Объем аккумулирующего бака
	</td>
	<td>
			л
	</td>
	<td>
			540
	</td>
	<td>
			800
	</td>
	<td>
			1000
	</td>
	<td>
			1300
	</td>
	<td>
			1495
	</td>
	<td>
			1650
	</td>
</tr>

<tr>
	<td>
			Рекомендуемая площадь отопления(уголь) 
	</td>
	<td>
			м2
	</td>
	<td>
			от 50 до 90
	</td>
	<td>
			от 91 до 160
	</td>
	<td>
			от 161 до 220
	</td>
	<td>
			от 221 до 290
	</td>
	<td>
			от 291 до 360
	</td>
	<td>
			от 361 до 430
	</td>
</tr>

<tr>
	<td>
			Рекомендуемая площадь отопления (дрова) 
	</td>
	<td>
			м2
	</td>
	<td>
			от 50 до 80
	</td>
	<td>
			от 81 до 150
	</td>
	<td>
			от 151 до 200
	</td>
	<td>
			от 201 до 270
	</td>
	<td>
			от 271 до 340
	</td>
	<td>
			от 341 до 410
	</td>
</tr>


</tbody>
</table>

<div class="col-xs-11">

<p>при условии влажности дров не более 20% </p>
<p>при более низкой температуре в топке образуется конденсат и смола, которые оказывают губительное действие на котел </p>
<p>при несоблюдении данного условия возникает опасность температурного шока, что приведет к разрушению чугунного теплообменника </p>

</div>	
<div style="text-align:right" class="col-xs-1">
<p>*</p>
<p>**</p>
<p>***</p>
</div>	
		</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>