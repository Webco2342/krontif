

<? 
define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(CModule::IncludeModule("iblock"))
$el = new CIBlockElement;

$PROP = array();
$PROP[EL_ID] = $_GET['el_id'];  


$arLoadProductArray = Array(
  "MODIFIED_BY"    => $USER->GetID(), 
  "IBLOCK_SECTION_ID" => false,         
  "IBLOCK_ID"      => 8,
  "PROPERTY_VALUES"=> $PROP,
  "CODE"           => $number,
  "NAME"           => $_GET['name'],
  "ACTIVE"         => "N",            
  "PREVIEW_TEXT"   => $_GET['text'],
  );

if($PRODUCT_ID = $el->Add($arLoadProductArray))
  echo '<p class="bg-success" style="padding:30px">Спасибо за ваш комментарий! Он появится на сайте после проверки модератором. </p>';
else
  echo "Error: ".$el->LAST_ERROR;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>